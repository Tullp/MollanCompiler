package page;

import mollan.Mollan;
import mollan.compilers.MollanException;
import mollan.impl.MollanImpl;
import mollan.runtime.RuntimeEnvironment;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.stream.Collectors;

@WebServlet(name = "APIServlet", value = "/execute")
public class APIServlet extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {

	    String source = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));

		Mollan mollan = new MollanImpl();

		RuntimeEnvironment runtimeEnvironment = new RuntimeEnvironment();

		String output;

		try {

			mollan.execute(source, runtimeEnvironment);

			output = runtimeEnvironment.getOutput();

		} catch (MollanException error) {

			output = error.getMessage();
		}

		PrintWriter printWriter = response.getWriter();

		printWriter.write(output);

		printWriter.close();
	}
}
