package page;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;

@WebServlet(name = "PageServlet", value = "/servletpage")
public class PageServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

		response.setContentType("text/html");

		PrintWriter printWriter = response.getWriter();

		printWriter.write(new String(Files.readAllBytes(Paths.get(
			"C:", "D", "java", "MollanCompiler", "servlets", "src", "main", "webapp", "index.jsp"
		))));

		printWriter.close();
	}
}
