package mollan;

import mollan.compilers.MollanException;

/**
 * Calculator API. Supports base math operators, doubles, functions and brackets.<br>
 */
public interface Calculator {

	double calculate(String inputString) throws MollanException;
}
