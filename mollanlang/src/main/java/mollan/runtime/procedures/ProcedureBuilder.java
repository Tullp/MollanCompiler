package mollan.runtime.procedures;

import mollan.runtime.RuntimeEnvironment;
import mollan.runtime.types.ValueHolder;

import java.util.List;
import java.util.function.BiConsumer;

/**
 * Build {@link Procedure}.<br>
 */
class ProcedureBuilder {

	private String name;

	private BiConsumer<RuntimeEnvironment, List<ValueHolder<?>>> consumer;

	private int minArgumentsCount = 0;

	private int maxArgumentsCount = Integer.MAX_VALUE;

	public ProcedureBuilder setName(String name) {

		this.name = name;

		return this;
	}

	ProcedureBuilder setConsumer(BiConsumer<RuntimeEnvironment, List<ValueHolder<?>>> consumer) {

		this.consumer = consumer;

		return this;
	}

	ProcedureBuilder setMinArgumentsCount(int count) {

		this.minArgumentsCount = count;

		return this;
	}

	ProcedureBuilder setMaxArgumentsCount(int count) {

		this.maxArgumentsCount = count;

		return this;
	}

	Procedure build() {

		return new Procedure(name, consumer, minArgumentsCount, maxArgumentsCount);
	}

}
