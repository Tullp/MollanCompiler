package mollan.runtime.procedures;

import mollan.runtime.RuntimeEnvironment;
import mollan.runtime.types.ValueHolder;

import java.util.List;
import java.util.function.BiConsumer;

/**
 * Mollan procedure. Work like function, but without return.<br>
 */
public class Procedure {

	private final String name;
	private final BiConsumer<RuntimeEnvironment, List<ValueHolder<?>>> consumer;
	private final int minArgumentsCount;
	private final int maxArgumentsCount;

	Procedure(String name, BiConsumer<RuntimeEnvironment, List<ValueHolder<?>>> consumer, int minArgumentsCount, int maxArgumentsCount) {
		this.name = name;
		this.consumer = consumer;
		this.minArgumentsCount = minArgumentsCount;
		this.maxArgumentsCount = maxArgumentsCount;
	}

	String getName() {
		return this.name;
	}

	public boolean isCorrectArgumentsCount(int count) {

		return count >= this.minArgumentsCount && count <= this.maxArgumentsCount;
	}

	public void call(RuntimeEnvironment runtimeEnvironment, List<ValueHolder<?>> arguments) {

		this.consumer.accept(runtimeEnvironment, arguments);
	}
}
