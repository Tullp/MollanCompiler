package mollan.runtime.procedures;

import mollan.runtime.RuntimeEnvironment;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Factory for procedures.<br>
 */
public class ProcedureFactory {

	private final Map<String, Procedure> procedureMap = new HashMap<>();

	public ProcedureFactory() {

		procedureMap.putAll(Stream.of(
			new ProcedureBuilder()
				.setName("print")
				.setMinArgumentsCount(1)
				.setConsumer(RuntimeEnvironment::print)
				.build()
		).collect(Collectors.toMap(Procedure::getName, procedure -> procedure)));
	}

	public Optional<Procedure> getProcedure(String name) {

		if (!procedureMap.containsKey(name)) { return Optional.empty(); }

		return Optional.of(procedureMap.get(name));
	}
}
