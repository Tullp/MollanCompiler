package mollan.runtime;

import mollan.runtime.operators.Operator;
import mollan.runtime.types.ValueHolder;

import java.util.ArrayDeque;
import java.util.Deque;

/**
 * Shunting Yard algorithm. Needs for calculate mathexpression with priorities consider.<br>
 */
public class SortingStation {

	private final Deque<Operator> operators = new ArrayDeque<>();
	private final Deque<ValueHolder<?>> operands = new ArrayDeque<>();

	public void pushOperand(ValueHolder<?> operand) {
		this.operands.push(operand);
	}

	public void pushOperator(Operator operator) {

		if (this.operators.isEmpty() || this.operators.getFirst().getPriority() < operator.getPriority()) {

			this.operators.push(operator);

		} else {

			calculateLastOperator();

			this.pushOperator(operator);
		}
	}

	public ValueHolder<?> getResult() {

		while (this.operators.size() > 0) {

			calculateLastOperator();
		}

		return this.operands.pop();
	}

	private void calculateLastOperator() {

		Operator operator = this.operators.pop();
		ValueHolder<?> rightArg = this.operands.pop();
		ValueHolder<?> leftArg = this.operands.pop();

		this.operands.push(operator.apply(leftArg, rightArg));
	}
}
