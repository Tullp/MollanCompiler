/**
 * Hold any type data. Needs for supporting many types.<br>
 */
package mollan.runtime.types;