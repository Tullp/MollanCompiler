package mollan.runtime.types;

/**
 * Basic data holder. Implementations need for support many types.<br>
 *
 * @param <T> type of holding data.<br>
 */
public abstract class ValueHolder<T> {

	private final T value;

	protected ValueHolder(T value) {

		this.value = value;
	}

	public T value() {

		return this.value;
	}

	public abstract void accept(ValueVisitor valueVisitor);
}
