package mollan.runtime.types;

/**
 * Basic visitor for {@link ValueHolder}. Implementations need for validate and get data from ValueHolder's.<br>
 */
public class ValueVisitor {

	private boolean isPresent = false;

	public boolean isPresent() {
		return isPresent;
	}

	protected void makePresent() { this.isPresent = true; }

	public void visit(double value) {

	}

	public void visit(boolean value) {

	}
}
