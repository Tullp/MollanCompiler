package mollan.runtime.types.booleantype;

import mollan.runtime.types.ValueHolder;
import mollan.runtime.types.ValueVisitor;

/**
 * ValueHolder implementation for support boolean type.<br>
 */
public class BooleanValueHolder extends ValueHolder<Boolean> {

	public BooleanValueHolder(Boolean value) {
		super(value);
	}

	@Override
	public void accept(ValueVisitor valueVisitor) {
		valueVisitor.visit(this.value());
	}
}
