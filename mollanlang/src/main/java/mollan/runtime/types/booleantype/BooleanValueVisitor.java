package mollan.runtime.types.booleantype;

import mollan.runtime.types.ValueHolder;
import mollan.runtime.types.ValueVisitor;

import java.util.Optional;

/**
 * ValueVisitor implementation for validate boolean type.<br>
 */
public class BooleanValueVisitor extends ValueVisitor {

	private boolean value;

	private boolean value() {

		return this.value;
	}

	@Override
	public void visit(boolean value) {

		this.makePresent();

		this.value = value;
	}

	public static Optional<Boolean> getBooleanValue(ValueHolder<?> valueHolder) {

		BooleanValueVisitor booleanValueVisitor = new BooleanValueVisitor();

		valueHolder.accept(booleanValueVisitor);

		if (booleanValueVisitor.isPresent()) {

			return Optional.of(booleanValueVisitor.value());

		} else {

			return Optional.empty();
		}
	}
}
