package mollan.runtime.types.doubletype;

import mollan.runtime.types.ValueHolder;
import mollan.runtime.types.ValueVisitor;

import java.util.Optional;

/**
 * ValueVisitor implementation for validate double type.<br>
 */
public class DoubleValueVisitor extends ValueVisitor {

	private double value;

	private double value() {

		return this.value;
	}

	@Override
	public void visit(double value) {

		this.makePresent();

		this.value = value;
	}

	public static Optional<Double> getDoubleValue(ValueHolder<?> valueHolder) {

		DoubleValueVisitor doubleValueVisitor = new DoubleValueVisitor();

		valueHolder.accept(doubleValueVisitor);

		if (doubleValueVisitor.isPresent()) {

			return Optional.of(doubleValueVisitor.value());

		} else {

			return Optional.empty();
		}
	}
}
