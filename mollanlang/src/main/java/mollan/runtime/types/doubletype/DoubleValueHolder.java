package mollan.runtime.types.doubletype;

import mollan.runtime.types.ValueHolder;
import mollan.runtime.types.ValueVisitor;

/**
 * ValueHolder implementation for support double type.<br>
 */
public class DoubleValueHolder extends ValueHolder<Double> {

	public DoubleValueHolder(Double value) {
		super(value);
	}

	@Override
	public void accept(ValueVisitor valueVisitor) {
		valueVisitor.visit(this.value());
	}
}
