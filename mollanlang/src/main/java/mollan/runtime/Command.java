package mollan.runtime;

import mollan.compilers.MollanException;

/**
 * Class, that calculate parts of mathexpression during runtime. All compilers return command.<br>
 */
public interface Command {

	void execute(RuntimeEnvironment runtimeEnvironment) throws MollanException;
}
