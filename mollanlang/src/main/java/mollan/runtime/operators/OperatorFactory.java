package mollan.runtime.operators;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.BiFunction;

/**
 * Factory for all operators. Used for bind operators with their string representation.<br>
 */
public class OperatorFactory {

	private final Map<String, DoubleToDoubleOperator> mathOperators = new HashMap<>();
	private final Map<String, DoubleToBooleanOperator> compareOperators = new HashMap<>();

	private void addMathOperator(BiFunction<Double, Double, Double> biFunction, String symbol) {

		DoubleToDoubleOperator operator = new DoubleToDoubleOperator(biFunction, mathOperators.size(), symbol);
		mathOperators.put(operator.getSymbol(), operator);
	}

	private void addCompareOperator(BiFunction<Double, Double, Boolean> biFunction, String symbol) {

		DoubleToBooleanOperator operator = new DoubleToBooleanOperator(biFunction, mathOperators.size(), symbol);
		compareOperators.put(operator.getSymbol(), operator);
	}
	
	public OperatorFactory() {

		this.addMathOperator(Double::sum, "+");

		this.addMathOperator((a, b) -> a - b, "-");

		this.addMathOperator((a, b) -> a * b, "*");

		this.addMathOperator((a, b) -> a / b, "/");

		this.addMathOperator(StrictMath::pow, "^");


		this.addCompareOperator((a, b) -> a >= b, ">=");

		this.addCompareOperator((a, b) -> a <= b, "<=");

		this.addCompareOperator((a, b) -> a > b, ">");

		this.addCompareOperator((a, b) -> a < b, "<");
	}

	public Operator getMathOperator(String operatorSymbol) {

		return this.mathOperators.get(operatorSymbol);
	}

	public Operator getCompareOperator(String operatorSymbol) {

		return this.compareOperators.get(operatorSymbol);
	}

	public Set<String> getMathOperators() { return this.mathOperators.keySet(); }

	public Set<String> getCompareOperators() { return this.compareOperators.keySet(); }
}
