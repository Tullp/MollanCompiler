package mollan.runtime.operators;

import mollan.runtime.types.ValueHolder;

/**
 * Operators API. Used during runtime for calculate mathexpression in SortingStation.<br>
 */
public abstract class Operator {

	private final int priority;
	private final String symbol;

	Operator(int priority, String symbol) {
		this.priority = priority;
		this.symbol = symbol;
	}

	public int getPriority() { return this.priority; }

	String getSymbol() { return this.symbol; }

	public abstract ValueHolder<?> apply(ValueHolder<?> leftArg, ValueHolder<?> rightArg);
}
