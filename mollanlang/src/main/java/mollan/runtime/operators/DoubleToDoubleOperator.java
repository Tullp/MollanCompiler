package mollan.runtime.operators;

import mollan.runtime.types.ValueHolder;
import mollan.runtime.types.doubletype.DoubleValueHolder;
import mollan.runtime.types.doubletype.DoubleValueVisitor;

import java.util.Optional;
import java.util.function.BiFunction;

public class DoubleToDoubleOperator extends Operator{

	private final BiFunction<Double, Double, Double> biFunction;

	DoubleToDoubleOperator(BiFunction<Double, Double, Double> biFunction, int priority, String symbol) {

		super(priority, symbol);
		this.biFunction = biFunction;
	}

	@Override
	public ValueHolder<?> apply(ValueHolder<?> leftArg, ValueHolder<?> rightArg) {

		Optional<Double> arg1 = DoubleValueVisitor.getDoubleValue(leftArg);
		Optional<Double> arg2 = DoubleValueVisitor.getDoubleValue(rightArg);

		if (!arg1.isPresent() || !arg2.isPresent()) {

			throw new RuntimeException(String.format(
				"Wrong types in '%s' operator. It take two doubles and return double.", getSymbol()
			));
		}

		return new DoubleValueHolder(this.biFunction.apply(arg1.get(), arg2.get()));
	}
}
