package mollan.runtime;

import mollan.runtime.types.ValueHolder;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Runtime environment for Mollan lang, contains all what we may need during executing commands ({@link Command}).<br>
 */
public final class RuntimeEnvironment {

	private final OutputStream outputStream = new ByteArrayOutputStream();

	private final Deque<SortingStation> sortingStationsStack = new ArrayDeque<>();

	private final Map<String, ValueHolder<?>> variables = new HashMap<>();

	public void openSortingStation() {

		this.sortingStationsStack.push(new SortingStation());
	}

	public SortingStation getSortingStation() {

		return this.sortingStationsStack.getFirst();
	}

	public void closeSortingStation() {

		this.sortingStationsStack.pop();
	}

	public void print(List<ValueHolder<?>> args) {

		if (this.outputStream.toString().isEmpty()) {

			new PrintStream(this.outputStream).print(args.get(0).value());

			args.remove(0);
		}

		args.forEach(arg -> new PrintStream(this.outputStream).print(", " + arg.value()));
	}

	public String getOutput() {

		return this.outputStream.toString();
	}

	public void setVariable(String name, ValueHolder<?> value) {

		this.variables.put(name, value);
	}

	public Optional<ValueHolder<?>> getVariable(String name) {

		if (this.variables.get(name) != null) {

			return Optional.of(this.variables.get(name));

		} else {

			return Optional.empty();
		}
	}
}
