package mollan.runtime.functions;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Factory for all function. Used for bind function with their string representation (names).<br>
 */
public class FunctionFactory {

	private final Map<String, MathFunction> functionsMap = new HashMap<>();

	public FunctionFactory() {

		functionsMap.putAll(Stream.of(

			new MathFunctionBuilder()
				.setName("abs")
				.setFunction(args -> Math.abs(args.get(0)))
				.setMinArgumentsCount(1)
				.setMaxArgumentsCount(1)
				.build(),

			new MathFunctionBuilder()
				.setName("min")
				.setFunction(Collections::min)
				.setMinArgumentsCount(1)
				.build(),

			new MathFunctionBuilder()
				.setName("max")
				.setFunction(Collections::max)
				.setMinArgumentsCount(1)
				.build(),

			new MathFunctionBuilder()
				.setName("sum")
				.setFunction(args -> args.stream().mapToDouble(Double::doubleValue).sum())
				.setMinArgumentsCount(1)
				.build(),

			new MathFunctionBuilder()
				.setName("pi")
				.setFunction(args -> Math.PI)
				.setMaxArgumentsCount(0)
				.build(),

			new MathFunctionBuilder()
				.setName("sqrt")
				.setFunction(args -> Math.sqrt(args.get(0)))
				.setMinArgumentsCount(1)
				.setMaxArgumentsCount(1)
				.build(),

			new MathFunctionBuilder()
				.setName("log2")
				.setFunction(args -> StrictMath.log(args.get(0)) / StrictMath.log(2))
				.setMinArgumentsCount(1)
				.setMaxArgumentsCount(1)
				.build()
		).collect(Collectors.toMap(MathFunction::getName, mathFunction -> mathFunction)));
	}

	public Optional<MathFunction> getFunction(String name) {

		if (!functionsMap.containsKey(name)) {

			return Optional.empty();
		}

		return Optional.of(functionsMap.get(name));
	}
}
