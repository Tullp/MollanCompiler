package mollan.runtime.functions;

import java.util.List;
import java.util.function.Function;

class MathFunctionBuilder {

	private String name;

	private Function<List<Double>, Double> function;

	private int minArgumentsCount = 0;

	private int maxArgumentsCount = Integer.MAX_VALUE;

	public MathFunctionBuilder setName(String name) {

		this.name = name;

		return this;
	}

	MathFunctionBuilder setFunction(Function<List<Double>, Double> function) {

		this.function = function;

		return this;
	}

	MathFunctionBuilder setMinArgumentsCount(int count) {

		this.minArgumentsCount = count;

		return this;
	}

	MathFunctionBuilder setMaxArgumentsCount(int count) {

		this.maxArgumentsCount = count;

		return this;
	}

	MathFunction build() {

		return new MathFunction(name, function, minArgumentsCount, maxArgumentsCount);
	}

}
