package mollan.runtime.functions;

import java.util.List;
import java.util.function.Function;

/**
 * Functions API. Used during runtime for calculate mathexpression.<br>
 */
public class MathFunction {

	private final String name;

	private final Function<List<Double>, Double> function;

	private final int minArgumentsCount;

	private final int maxArgumentsCount;

	MathFunction(String name, Function<List<Double>, Double> function, int minArgumentsCount, int maxArgumentsCount) {
		this.name = name;
		this.function = function;
		this.minArgumentsCount = minArgumentsCount;
		this.maxArgumentsCount = maxArgumentsCount;
	}

	String getName() {
		return this.name;
	}

	public boolean checkArgumentsCount(int count) {

		return count >= this.minArgumentsCount && count <= this.maxArgumentsCount;
	}

	public double calculate(List<Double> arguments) {

		return this.function.apply(arguments);
	}
}
