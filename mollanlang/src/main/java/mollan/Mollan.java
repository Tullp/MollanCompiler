package mollan;

import mollan.compilers.MollanException;
import mollan.runtime.RuntimeEnvironment;

/**
 * Mollan lang compiler. Weakly typed lang, wrote by Maks in 2021.<br>
 * Support statement with variable initialization, procedures, 'switch' statement and 'for' loop.<br>
 *
 * Example: a = (1 + 5) * 3; b = 2 * a; for (i = 0; i < a; i = i + 1) { print(i); };
 */
public interface Mollan {

	void execute(String inputString, RuntimeEnvironment runtimeEnvironment) throws MollanException;
}
