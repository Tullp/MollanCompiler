package mollan.impl;

import mollan.Calculator;
import mollan.compilers.CompilerFactory;
import mollan.compilers.CompilerType;
import mollan.compilers.InputChain;
import mollan.compilers.MollanCompileException;
import mollan.compilers.MollanException;
import mollan.runtime.Command;
import mollan.runtime.RuntimeEnvironment;
import mollan.runtime.types.doubletype.DoubleValueVisitor;

import java.util.Optional;

public class CalculatorImpl implements Calculator {

	@Override
	public double calculate(String inputString) throws MollanException {

		InputChain inputChain = new InputChain(inputString);

		CompilerFactory compilerFactory = new CompilerFactoryImpl();

		Optional<Command> command = compilerFactory
			.create(CompilerType.MATHEXPRESSION)
			.compile(inputChain);

		if (command.isPresent()) {

			RuntimeEnvironment runtimeEnvironment = new RuntimeEnvironment();

			runtimeEnvironment.openSortingStation();

			command.get().execute(runtimeEnvironment);

			Optional<Double> result = DoubleValueVisitor.getDoubleValue(runtimeEnvironment.getSortingStation().getResult());

			if (!result.isPresent()) {

				throw new MollanCompileException("Calculator supports only double.", inputChain.getPointer());
			}

			return result.get();
		}

		throw new MollanCompileException("Invalid syntax", inputChain.getPointer());
	}
}
