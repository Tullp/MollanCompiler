package mollan.impl;

import mollan.Mollan;
import mollan.compilers.CompilerFactory;
import mollan.compilers.CompilerType;
import mollan.compilers.InputChain;
import mollan.compilers.MollanCompileException;
import mollan.compilers.MollanException;
import mollan.runtime.Command;
import mollan.runtime.RuntimeEnvironment;

import java.util.Optional;

public class MollanImpl implements Mollan {

	@Override
	public void execute(String inputString, RuntimeEnvironment runtimeEnvironment) throws MollanException {

		InputChain inputChain = new InputChain(inputString);

		CompilerFactory compilerFactory = new CompilerFactoryImpl();

		Optional<Command> command = compilerFactory
			.create(CompilerType.PROGRAM)
			.compile(inputChain);

		if (command.isPresent()) {

			command.get().execute(runtimeEnvironment);

		} else {

			throw new MollanCompileException("Invalid syntax.", inputChain.getPointer());
		}
	}
}
