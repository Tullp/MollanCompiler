package mollan.impl;

import mollan.compilers.Compiler;
import mollan.compilers.CompilerFactory;
import mollan.compilers.CompilerType;
import mollan.compilers.fsm.boolexpression.BoolExpressionCompiler;
import mollan.compilers.fsm.codeblock.CodeBlockCompiler;
import mollan.compilers.fsm.expression.ExpressionCompiler;
import mollan.compilers.fsm.forloop.ForLoopCompiler;
import mollan.compilers.fsm.function.FunctionCompiler;
import mollan.compilers.fsm.ifstatement.IfStatementCompiler;
import mollan.compilers.fsm.mathcalculable.MathCalculableCompiler;
import mollan.compilers.fsm.mathexpression.MathExpressionCompiler;
import mollan.compilers.fsm.number.NumberCompiler;
import mollan.compilers.fsm.procedure.ProcedureCompiler;
import mollan.compilers.fsm.program.ProgramCompiler;
import mollan.compilers.fsm.statement.StatementCompiler;
import mollan.compilers.fsm.switchoperator.SwitchCompiler;
import mollan.compilers.fsm.variables.InitVariableCompiler;
import mollan.compilers.fsm.variables.VariableCompiler;

import java.util.EnumMap;
import java.util.Map;

public class CompilerFactoryImpl implements CompilerFactory {

	private final Map<CompilerType, Compiler> compilers = new EnumMap<>(CompilerType.class);

	CompilerFactoryImpl() {

		this.configure();
	}

	private void configure() {

		this.compilers.put(CompilerType.NUMBER, new NumberCompiler(this));
		this.compilers.put(CompilerType.FUNCTION, new FunctionCompiler(this));
		this.compilers.put(CompilerType.VARIABLE, new VariableCompiler(this));
		this.compilers.put(CompilerType.MATHCALCULABLE, new MathCalculableCompiler(this));
		this.compilers.put(CompilerType.MATHEXPRESSION, new MathExpressionCompiler(this));
		this.compilers.put(CompilerType.BOOLEXPRESSION, new BoolExpressionCompiler(this));
		this.compilers.put(CompilerType.EXPRESSION, new ExpressionCompiler(this));
		this.compilers.put(CompilerType.INITVARIABLE, new InitVariableCompiler(this));
		this.compilers.put(CompilerType.PROCEDURE, new ProcedureCompiler(this));
		this.compilers.put(CompilerType.CODEBLOCK, new CodeBlockCompiler(this));
		this.compilers.put(CompilerType.FORLOOP, new ForLoopCompiler(this));
		this.compilers.put(CompilerType.SWITCH, new SwitchCompiler(this));
		this.compilers.put(CompilerType.IFSTATEMENT, new IfStatementCompiler(this));
		this.compilers.put(CompilerType.STATEMENT, new StatementCompiler(this));
		this.compilers.put(CompilerType.PROGRAM, new ProgramCompiler(this));
	}
	
	@Override
	public Compiler create(CompilerType compilerType) {

		return this.compilers.get(compilerType);
	}
}
