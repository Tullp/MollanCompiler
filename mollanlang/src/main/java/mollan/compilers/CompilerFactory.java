package mollan.compilers;

/**
 * CompilerFactory API. Used for generate compilers. Needs for avoid cyclic dependencies.<br>
 */
public interface CompilerFactory {

	Compiler create(CompilerType compilerType);
}
