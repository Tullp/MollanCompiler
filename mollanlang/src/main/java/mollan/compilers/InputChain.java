package mollan.compilers;

/**
 * Class, that contains string with mathexpression. Used in compilers for compile this mathexpression.<br>
 */
public class InputChain {
	private final String data;
	private int pointer;

	public InputChain(String data) {
		this.data = data;
		this.pointer = 0;
	}

	public char getCurrent() {
		return this.data.charAt(this.pointer);
	}

	public String getPart(int start, int offset) {

		if (this.data.length() < start + offset) {

			return this.data.substring(start);
		}

		return this.data.substring(start, start + offset);
	}

	public void increasePointer(int value) {
		this.pointer += value;
	}

	public int getPointer() {
		return this.pointer;
	}

	public boolean isEnd() {
		return this.pointer >= this.data.length();
	}

	public void setPointer(int pointer) {
		this.pointer = pointer;
	}

}
