package mollan.compilers;

public class MollanCompileException extends MollanException {
	private static final long serialVersionUID = 6328103606448893936L;

	public MollanCompileException(String message, int position) {
		super(message, position);
	}
}
