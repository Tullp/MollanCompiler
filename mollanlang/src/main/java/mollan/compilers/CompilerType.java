package mollan.compilers;

/**
 * Compilers types. Used in CompilerFactory.<br>
 */
public enum CompilerType {

	NUMBER,
	FUNCTION,
	VARIABLE,
	MATHCALCULABLE,
	MATHEXPRESSION,
	BOOLEXPRESSION,
	EXPRESSION,
	INITVARIABLE,
	PROCEDURE,
	CODEBLOCK,
	FORLOOP,
	SWITCH,
	IFSTATEMENT,
	STATEMENT,
	PROGRAM
}
