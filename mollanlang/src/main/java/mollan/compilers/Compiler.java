package mollan.compilers;

import mollan.runtime.Command;

import java.util.Optional;

/**
 * Wrapper for machines. Needs for avoid output type addiction and split compilation and runtime calculation.<br>
 */
public abstract class Compiler {

	protected CompilerFactory compilerFactory;

	protected Compiler(CompilerFactory compilerFactory) {

		this.compilerFactory = compilerFactory;
	}

	public abstract Optional<Command> compile(InputChain inputChain) throws MollanCompileException;
}
