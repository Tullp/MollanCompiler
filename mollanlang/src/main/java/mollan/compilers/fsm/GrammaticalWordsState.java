package mollan.compilers.fsm;

import mollan.compilers.InputChain;

import java.util.ArrayList;
import java.util.Collection;
import java.util.function.BiConsumer;

public class GrammaticalWordsState<O> extends State<O> {

	private final Collection<String> words = new ArrayList<>();
	private final BiConsumer<O, String> consumer;

	GrammaticalWordsState(Collection<String> words, BiConsumer<O, String> consumer, boolean isFinish) {

		super(isFinish);
		this.words.addAll(words);
		this.consumer = consumer;
	}

	@Override
	public boolean tryTransition(InputChain inputChain, O outputChain) {

		for (String word : this.words) {

			if (inputChain.getPart(inputChain.getPointer(), word.length()).equals(word)) {

				inputChain.increasePointer(word.length());

				this.consumer.accept(outputChain, word);

				return true;
			}
		}

		return false;
	}
}
