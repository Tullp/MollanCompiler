package mollan.compilers.fsm.boolexpression;

import mollan.compilers.CompilerFactory;
import mollan.compilers.CompilerType;
import mollan.compilers.fsm.CompilerWrapperState;
import mollan.compilers.fsm.CompilerWrapperStateBuilder;
import mollan.compilers.fsm.FiniteStateMachine;
import mollan.compilers.fsm.GrammaticalWordsState;
import mollan.compilers.fsm.GrammaticalWordsStateBuilder;
import mollan.compilers.fsm.State;
import mollan.runtime.Command;
import mollan.runtime.operators.OperatorFactory;
import mollan.runtime.types.ValueHolder;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Implementation of {@link FiniteStateMachine} that is used for parse boolean expressions.<br>
 * See {@link BoolExpressionCompiler}.<br>
 */
public class BoolExpressionMachine extends FiniteStateMachine<List<Command>> {

	public static BoolExpressionMachine create(CompilerFactory compilerFactory) {

		CompilerWrapperStateBuilder<List<Command>> expressionStateBuilder = new CompilerWrapperStateBuilder<List<Command>>(compilerFactory)
			.setCompilerType(CompilerType.MATHEXPRESSION)
			.setResultAdapter((outputChain, command) -> outputChain.add(runtimeEnvironment -> {

					runtimeEnvironment.openSortingStation();

					command.execute(runtimeEnvironment);

					ValueHolder<?> result = runtimeEnvironment.getSortingStation().getResult();

					runtimeEnvironment.closeSortingStation();

					runtimeEnvironment.getSortingStation().pushOperand(result);
			}));

		CompilerWrapperState<List<Command>> firstExpressionState = expressionStateBuilder
			.build();

		CompilerWrapperState<List<Command>> secondExpressionState = expressionStateBuilder
			.makeFinish()
			.build();

		GrammaticalWordsState<List<Command>> operatorState = new GrammaticalWordsStateBuilder<List<Command>>()
			.setWords(new OperatorFactory().getCompareOperators()
				.stream()
				.sorted((s1, s2) -> s2.length() - s1.length())
				.collect(Collectors.toList())
			).setConsumer((commands, symbol) -> commands.add(runtimeEnvironment ->
				runtimeEnvironment.getSortingStation().pushOperator(
					new OperatorFactory().getCompareOperator(symbol)
				)
			)).build();

		firstExpressionState.addTransition(operatorState);
		operatorState.addTransition(secondExpressionState);

		return new BoolExpressionMachine(firstExpressionState);
	}

	@SafeVarargs
	private BoolExpressionMachine(State<List<Command>>... startStates) { super(startStates); }
}
