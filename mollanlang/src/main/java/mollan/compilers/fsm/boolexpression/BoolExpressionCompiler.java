package mollan.compilers.fsm.boolexpression;

import mollan.compilers.Compiler;
import mollan.compilers.CompilerFactory;
import mollan.compilers.InputChain;
import mollan.compilers.MollanCompileException;
import mollan.runtime.Command;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Implementation of {@link Compiler}, that compiles boolean expressions.<br>
 * Boolean expression is combination of math expression, boolean operator and math expression.<br>
 * More info about math expressions: {@link mollan.compilers.fsm.mathexpression.MathExpressionMachine}.<br>
 * More info about boolean operators: {@link mollan.runtime.operators.OperatorFactory}.<br>
 * The compiler is based on {@link BoolExpressionMachine}.<br>
 */
public class BoolExpressionCompiler extends Compiler {

	public BoolExpressionCompiler(CompilerFactory compilerFactory) { super(compilerFactory); }

	@Override
	public Optional<Command> compile(InputChain inputChain) throws MollanCompileException {

		List<Command> commands = new ArrayList<>();

		if (BoolExpressionMachine.create(compilerFactory).execute(inputChain, commands, false)) {

			return Optional.of(runtimeEnvironment -> {

				for (Command command : commands) {

					command.execute(runtimeEnvironment);
				}
			});
		}

		return Optional.empty();

	}
}
