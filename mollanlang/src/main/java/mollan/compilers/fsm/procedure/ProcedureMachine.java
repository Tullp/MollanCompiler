package mollan.compilers.fsm.procedure;

import mollan.compilers.CompilerFactory;
import mollan.compilers.CompilerType;
import mollan.compilers.fsm.CompilerWrapperState;
import mollan.compilers.fsm.CompilerWrapperStateBuilder;
import mollan.compilers.fsm.FiniteStateMachine;
import mollan.compilers.fsm.SingleCharacterState;
import mollan.compilers.fsm.SingleCharacterStateBuilder;
import mollan.compilers.fsm.State;

/**
 * Implementation of {@link FiniteStateMachine} that is used for parse procedures.<br>
 * See {@link ProcedureCompiler}.<br>
 */
public class ProcedureMachine extends FiniteStateMachine<ProcedureContext> {
	
	public static ProcedureMachine create(CompilerFactory compilerFactory) {
		SingleCharacterState<ProcedureContext> letterState = new SingleCharacterStateBuilder<ProcedureContext>()
			.setCharacterPredicate(Character::isLetter)
			.setResultAdapter(ProcedureContext::addNameCharacter)
			.build();

		SingleCharacterState<ProcedureContext> digitState = new SingleCharacterStateBuilder<ProcedureContext>()
			.setCharacterPredicate(Character::isDigit)
			.setResultAdapter(ProcedureContext::addNameCharacter)
			.build();

		CompilerWrapperState<ProcedureContext> argumentState = new CompilerWrapperStateBuilder<ProcedureContext>(compilerFactory)
			.setCompilerType(CompilerType.EXPRESSION)
			.setResultAdapter(ProcedureContext::addArgumentCommand)
			.build();

		SingleCharacterState<ProcedureContext> openBracketState = new SingleCharacterStateBuilder<ProcedureContext>()
			.setCharacterPredicate(character -> character == '(')
			.build();

		SingleCharacterState<ProcedureContext> closeBracketState = new SingleCharacterStateBuilder<ProcedureContext>()
			.setCharacterPredicate(character -> character == ')')
			.makeFinish()
			.build();

		SingleCharacterState<ProcedureContext> commaState = new SingleCharacterStateBuilder<ProcedureContext>()
			.setCharacterPredicate(character -> character == ',')
			.build();

		letterState.addTransition(letterState, digitState, openBracketState);
		digitState.addTransition(digitState, letterState, openBracketState);
		openBracketState.addTransition(argumentState, closeBracketState);
		argumentState.addTransition(commaState, closeBracketState);
		commaState.addTransition(argumentState);

		return new ProcedureMachine(letterState);
	}

	@SafeVarargs
	private ProcedureMachine(State<ProcedureContext>... states) { super(states); }
}
