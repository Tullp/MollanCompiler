package mollan.compilers.fsm.procedure;

import mollan.compilers.Compiler;
import mollan.compilers.CompilerFactory;
import mollan.compilers.InputChain;
import mollan.compilers.MollanCompileException;
import mollan.runtime.Command;
import mollan.runtime.procedures.Procedure;
import mollan.runtime.procedures.ProcedureFactory;
import mollan.runtime.types.ValueHolder;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Implementation of {@link Compiler}, that compiles procedures.<br>
 * Procedure is function without return. Procedure is part of statements.<br>
 * More about statements: {@link mollan.compilers.fsm.statement.StatementCompiler}.<br>
 * Procedure: {@link Procedure}.<br>
 * Procedures list: {@link ProcedureFactory}.<br>
 * The compiler is based on {@link ProcedureMachine}.<br>
 */
public class ProcedureCompiler extends Compiler {

	public ProcedureCompiler(CompilerFactory compilerFactory) { super(compilerFactory); }

	@Override
	public Optional<Command> compile(InputChain inputChain) throws MollanCompileException {

		ProcedureContext procedureContext = new ProcedureContext();

		if (ProcedureMachine.create(compilerFactory).execute(inputChain, procedureContext, false)) {

			Optional<Procedure> procedure = new ProcedureFactory().getProcedure(procedureContext.getProcedureName());

			if (!procedure.isPresent()) {

				throw new MollanCompileException(
					String.format("Procedure with name '%s' doesn't exist.", procedureContext.getProcedureName()),
					inputChain.getPointer()
				);
			}

			if (!procedure.get().isCorrectArgumentsCount(procedureContext.getProcedureArgumentsCommands().size())) {

				throw new MollanCompileException(
					String.format("Procedure '%s' doesn't take %d arguments.",
						procedureContext.getProcedureName(),
						procedureContext.getProcedureArgumentsCommands().size()),
					inputChain.getPointer()
				);
			}

			return Optional.of(runtimeEnvironment -> {

				List<ValueHolder<?>> arguments = new ArrayList<>();

				for (Command command : procedureContext.getProcedureArgumentsCommands()) {

					runtimeEnvironment.openSortingStation();

					command.execute(runtimeEnvironment);

					arguments.add(runtimeEnvironment.getSortingStation().getResult());

					runtimeEnvironment.closeSortingStation();
				}

				procedure.get().call(runtimeEnvironment, arguments);
			});
		}

		return Optional.empty();
	}
}
