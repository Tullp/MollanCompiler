package mollan.compilers.fsm.procedure;

import mollan.runtime.Command;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/**
 * This context accumulate procedure name and arguments.<br>
 */
class ProcedureContext {

	private final StringBuilder procedureName = new StringBuilder(20);
	private final Collection<Command> procedureArgumentsCommands = new ArrayList<>();

	void addNameCharacter(Character character) {
		this.procedureName.append(character);
	}

	void addArgumentCommand(Command argumentCommand) {

		this.procedureArgumentsCommands.add(argumentCommand);
	}

	String getProcedureName() {

		return this.procedureName.toString();
	}

	public Collection<Command> getProcedureArgumentsCommands() {

		return Collections.unmodifiableCollection(this.procedureArgumentsCommands);
	}
}
