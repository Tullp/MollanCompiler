package mollan.compilers.fsm.expression;

import mollan.compilers.CompilerFactory;
import mollan.compilers.CompilerType;
import mollan.compilers.fsm.CompilerWrapperState;
import mollan.compilers.fsm.CompilerWrapperStateBuilder;
import mollan.compilers.fsm.FiniteStateMachine;
import mollan.compilers.fsm.State;
import mollan.runtime.Command;

import java.util.List;

/**
 * Implementation of {@link FiniteStateMachine} that is used for parse expressions.<br>
 * See {@link ExpressionCompiler}.<br>
 */
public class ExpressionMachine extends FiniteStateMachine<List<Command>> {

	public static ExpressionMachine create(CompilerFactory compilerFactory) {

		CompilerWrapperState<List<Command>> boolExpressionState = new CompilerWrapperStateBuilder<List<Command>>(compilerFactory)
			.setCompilerType(CompilerType.BOOLEXPRESSION)
			.setResultAdapter(List::add)
			.makeFinish()
			.build();

		CompilerWrapperState<List<Command>> mathExpressionState = new CompilerWrapperStateBuilder<List<Command>>(compilerFactory)
			.setCompilerType(CompilerType.MATHEXPRESSION)
			.setResultAdapter(List::add)
			.makeFinish()
			.build();

		return new ExpressionMachine(boolExpressionState, mathExpressionState);
	}

	@SafeVarargs
	private ExpressionMachine(State<List<Command>>... startStates) { super(startStates); }
}
