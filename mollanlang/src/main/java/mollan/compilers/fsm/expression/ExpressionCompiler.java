package mollan.compilers.fsm.expression;

import mollan.compilers.Compiler;
import mollan.compilers.CompilerFactory;
import mollan.compilers.InputChain;
import mollan.compilers.MollanCompileException;
import mollan.runtime.Command;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Implementation of {@link Compiler}, that compiles expressions.<br>
 * Expression may be math or boolean.<br>
 * More about math expressions: {@link mollan.compilers.fsm.mathexpression.MathExpressionCompiler}.<br>
 * More about boolean expressions: {@link mollan.compilers.fsm.boolexpression.BoolExpressionCompiler}.<br>
 * The compiler is based on {@link ExpressionMachine}.<br>
 */
public class ExpressionCompiler extends Compiler {

	public ExpressionCompiler(CompilerFactory compilerFactory) { super(compilerFactory); }

	@Override
	public Optional<Command> compile(InputChain inputChain) throws MollanCompileException {

		List<Command> commands = new ArrayList<>();

		if (ExpressionMachine.create(compilerFactory).execute(inputChain, commands)) {

			return Optional.of(runtimeEnvironment -> {

				for (Command command : commands) {

					command.execute(runtimeEnvironment);
				}
			});
		}

		return Optional.empty();

	}
}
