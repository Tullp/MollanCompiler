package mollan.compilers.fsm;

import mollan.compilers.InputChain;
import mollan.compilers.MollanCompileException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;

/**
 * Represent a concept of <a href="en.wikipedia.org/wiki/Finite-state_machine">finite state machine</a>.<br>
 *
 * @param <O> type of output chain in terms of finite state machine.<br>
 */
public class FiniteStateMachine<O> {

	private final Collection<State<O>> startStates = new ArrayList<>();

	@SafeVarargs
	protected FiniteStateMachine(State<O>... startStates) { this.startStates.addAll(Arrays.asList(startStates)); }

	public boolean execute(InputChain inputChain, O outputChain) throws MollanCompileException {

		return this.execute(inputChain, outputChain, true);
	}

	public boolean execute(InputChain inputChain, O outputChain, boolean throwException) throws MollanCompileException {

		int startPointer = inputChain.getPointer();

		Optional<State<O>> startState = getNextState(this.startStates, inputChain, outputChain);

		if (!startState.isPresent()) {

			return false;
		}

		State<O> finishState = getFinishState(startState.get(), inputChain, outputChain);

		if (finishState.isFinish()) {

			return true;

		} else if (throwException) {

			throw new MollanCompileException("Invalid syntax from " + this.getClass().getSimpleName(), inputChain.getPointer());

		} else {

			inputChain.setPointer(startPointer);

			return false;
		}
	}

	private static void skipWhitespaces(InputChain inputChain) {

		// '\n', '\r', ' '.
		while (Arrays.asList(10, 13, 32).contains((int)inputChain.getCurrent())) {

			inputChain.increasePointer(1);
		}
	}

	private Optional<State<O>> getNextState(Iterable<State<O>> transitions, InputChain inputChain, O outputChain) throws MollanCompileException {

		for (State<O> state : transitions) {

			skipWhitespaces(inputChain);

			if (state.tryTransition(inputChain, outputChain)) {

				return Optional.of(state);
			}
		}

		return Optional.empty();
	}

	private State<O> getFinishState(State<O> startState, InputChain inputChain, O outputChain) throws MollanCompileException {

		State<O> currentState = startState;

		while (!inputChain.isEnd()) {

			Optional<State<O>> nextState = getNextState(currentState.getTransitions(), inputChain, outputChain);

			if (!nextState.isPresent()) {

				return currentState;
			}

			currentState = nextState.get();
		}

		return currentState;
	}
}
