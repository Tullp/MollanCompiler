package mollan.compilers.fsm;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.BiConsumer;

public class GrammaticalWordsStateBuilder<O> {

	private boolean isFinish = false;
	private final List<String> words = new ArrayList<>();
	private BiConsumer<O, String> consumer;

	public GrammaticalWordsStateBuilder<O> makeFinish() {

		this.isFinish = true;

		return this;
	}

	public GrammaticalWordsStateBuilder<O> setWords(Collection<String> words) {

		this.words.addAll(words);

		return this;
	}

	public GrammaticalWordsStateBuilder<O> setConsumer(BiConsumer<O, String> consumer) {

		this.consumer = consumer;

		return this;
	}

	public GrammaticalWordsState<O> build() {

		return new GrammaticalWordsState<>(words, consumer, isFinish);
	}
}
