package mollan.compilers.fsm.forloop;

import mollan.compilers.Compiler;
import mollan.compilers.CompilerFactory;
import mollan.compilers.InputChain;
import mollan.compilers.MollanCompileException;
import mollan.compilers.MollanException;
import mollan.runtime.Command;
import mollan.runtime.RuntimeEnvironment;
import mollan.runtime.types.booleantype.BooleanValueVisitor;

import java.util.Optional;

/**
 * Implementation of {@link Compiler}, that compiles 'for' loop.<br>
 * 'for' loop consists of head and body.<br>
 * 'for' loop's head consists of variable initialization, iteration condition and iteration action.<br>
 * 'for' loop's body - block of code, that executing at every iteration.<br>
 * More about 'for' loop: <a href="https://en.wikipedia.org/wiki/For_loop">wiki</a>.<br>
 * More about variables initialization: {@link mollan.compilers.fsm.variables.InitVariableCompiler}.<br>
 * More about iteration condition: {@link mollan.compilers.fsm.boolexpression.BoolExpressionCompiler}.<br>
 * More about iteration action: {@link mollan.compilers.fsm.variables.InitVariableCompiler}.<br>
 * More about blocks of code: {@link mollan.compilers.fsm.codeblock.CodeBlockCompiler}.<br>
 * The compiler is based on {@link ForLoopMachine}.<br>
 */
public class ForLoopCompiler extends Compiler {

	public ForLoopCompiler(CompilerFactory compilerFactory) { super(compilerFactory); }

	@Override
	public Optional<Command> compile(InputChain inputChain) throws MollanCompileException {

		ForLoopContext forLoopContext = new ForLoopContext();

		if (ForLoopMachine.create(compilerFactory).execute(inputChain, forLoopContext)) {

			return Optional.of(runtimeEnvironment -> {

				forLoopContext.getInitVariableCommand().execute(runtimeEnvironment);

				while (checkLoopCondition(runtimeEnvironment, forLoopContext.getLoopConditionCommand())) {

					forLoopContext.getLoopBodyCommand().execute(runtimeEnvironment);

					forLoopContext.getIterationCommand().execute(runtimeEnvironment);
				}
			});
		}

		return Optional.empty();

	}

	private static boolean checkLoopCondition(RuntimeEnvironment runtimeEnvironment, Command command) throws MollanException {

		runtimeEnvironment.openSortingStation();

		command.execute(runtimeEnvironment);

		Optional<Boolean> result = BooleanValueVisitor.getBooleanValue(runtimeEnvironment.getSortingStation().getResult());

		runtimeEnvironment.closeSortingStation();

		if (!result.isPresent()) {

			throw new RuntimeException("Wrong expression in for loop. Required boolean type.");
		}

		return result.get();
	}
}
