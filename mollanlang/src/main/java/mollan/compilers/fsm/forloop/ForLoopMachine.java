package mollan.compilers.fsm.forloop;

import mollan.compilers.CompilerFactory;
import mollan.compilers.CompilerType;
import mollan.compilers.fsm.CompilerWrapperStateBuilder;
import mollan.compilers.fsm.FiniteStateMachine;
import mollan.compilers.fsm.GrammaticalWordStateBuilder;
import mollan.compilers.fsm.State;

/**
 * Implementation of {@link FiniteStateMachine} that is used for parse 'for' loop.<br>
 * See {@link ForLoopCompiler}.<br>
 */
public class ForLoopMachine extends FiniteStateMachine<ForLoopContext> {

	public static ForLoopMachine create(CompilerFactory compilerFactory) {

		State<ForLoopContext> forWordState = new GrammaticalWordStateBuilder<ForLoopContext>()
			.setWord("for")
			.build();

		State<ForLoopContext> openBracketState = new GrammaticalWordStateBuilder<ForLoopContext>()
			.setWord("(")
			.build();

		State<ForLoopContext> closeBracketState = new GrammaticalWordStateBuilder<ForLoopContext>()
			.setWord(")")
			.build();

		GrammaticalWordStateBuilder<ForLoopContext> semicolonStateBuilder = new GrammaticalWordStateBuilder<ForLoopContext>()
			.setWord(";");

		State<ForLoopContext> firstSemicolonState = semicolonStateBuilder.build();

		State<ForLoopContext> secondSemicolonState = semicolonStateBuilder.build();

		State<ForLoopContext> initVariableState = new CompilerWrapperStateBuilder<ForLoopContext>(compilerFactory)
			.setCompilerType(CompilerType.INITVARIABLE)
			.setResultAdapter(ForLoopContext::setInitVariableCommand)
			.build();

		State<ForLoopContext> loopConditionState = new CompilerWrapperStateBuilder<ForLoopContext>(compilerFactory)
			.setCompilerType(CompilerType.BOOLEXPRESSION)
			.setResultAdapter(ForLoopContext::setLoopConditionCommand)
			.build();

		State<ForLoopContext> iterationActionState = new CompilerWrapperStateBuilder<ForLoopContext>(compilerFactory)
			.setCompilerType(CompilerType.INITVARIABLE)
			.setResultAdapter(ForLoopContext::setIterationCommand)
			.build();

		State<ForLoopContext> loopBodyState = new CompilerWrapperStateBuilder<ForLoopContext>(compilerFactory)
			.setCompilerType(CompilerType.CODEBLOCK)
			.setResultAdapter(ForLoopContext::setLoopBodyCommand)
			.makeFinish()
			.build();

		forWordState.addTransition(openBracketState);
		openBracketState.addTransition(initVariableState);
		initVariableState.addTransition(firstSemicolonState);
		firstSemicolonState.addTransition(loopConditionState);
		loopConditionState.addTransition(secondSemicolonState);
		secondSemicolonState.addTransition(iterationActionState);
		iterationActionState.addTransition(closeBracketState);
		closeBracketState.addTransition(loopBodyState);

		return new ForLoopMachine(forWordState);
	}

	@SafeVarargs
	private ForLoopMachine(State<ForLoopContext>... startStates) {
		super(startStates);
	}
}
