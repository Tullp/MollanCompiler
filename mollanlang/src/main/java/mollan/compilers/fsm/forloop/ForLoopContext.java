package mollan.compilers.fsm.forloop;

import mollan.runtime.Command;

/**
 * Output chain for {@link ForLoopMachine}.<br>
 */
class ForLoopContext {
	
	private Command initVariableCommand;
	private Command loopConditionCommand;
	private Command iterationCommand;
	private Command loopBodyCommand;

	void setInitVariableCommand(Command initVariableCommand) {

		this.initVariableCommand = initVariableCommand;
	}

	void setLoopConditionCommand(Command loopConditionCommand) {
		
		this.loopConditionCommand = loopConditionCommand;
	}

	void setIterationCommand(Command iterationCommand) {
		
		this.iterationCommand = iterationCommand;
	}

	void setLoopBodyCommand(Command loopBodyCommand) {
		
		this.loopBodyCommand = loopBodyCommand;
	}

	Command getInitVariableCommand() {
		
		return initVariableCommand;
	}

	Command getLoopConditionCommand() {
		
		return loopConditionCommand;
	}

	Command getIterationCommand() {
		
		return iterationCommand;
	}

	Command getLoopBodyCommand() {
		
		return loopBodyCommand;
	}
}
