package mollan.compilers.fsm.variables;

import mollan.compilers.CompilerFactory;
import mollan.compilers.CompilerType;
import mollan.compilers.fsm.CompilerWrapperState;
import mollan.compilers.fsm.CompilerWrapperStateBuilder;
import mollan.compilers.fsm.FiniteStateMachine;
import mollan.compilers.fsm.SingleCharacterState;
import mollan.compilers.fsm.SingleCharacterStateBuilder;
import mollan.compilers.fsm.State;

/**
 * Implementation of {@link FiniteStateMachine} that is used for parse variable initializations.<br>
 * See {@link InitVariableCompiler}.<br>
 */
public class InitVariableMachine extends FiniteStateMachine<InitVariableContext> {

	public static InitVariableMachine create(CompilerFactory compilerFactory) {

		SingleCharacterState<InitVariableContext> letterState = new SingleCharacterStateBuilder<InitVariableContext>()
			.setCharacterPredicate(Character::isLetter)
			.setResultAdapter(InitVariableContext::addNameCharacter)
			.build();

		SingleCharacterState<InitVariableContext> equalsState = new SingleCharacterStateBuilder<InitVariableContext>()
			.setCharacterPredicate(character -> character == '=')
			.build();

		CompilerWrapperState<InitVariableContext> expressionState = new CompilerWrapperStateBuilder<InitVariableContext>(compilerFactory)
			.setCompilerType(CompilerType.EXPRESSION)
			.setResultAdapter(InitVariableContext::setValueCommand)
			.makeFinish()
			.build();

		letterState.addTransition(letterState, equalsState);
		equalsState.addTransition(expressionState);

		return new InitVariableMachine(letterState);
	}

	@SafeVarargs
	private InitVariableMachine(State<InitVariableContext>... states) { super(states); }
}
