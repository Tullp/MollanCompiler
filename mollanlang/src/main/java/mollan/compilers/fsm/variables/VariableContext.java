package mollan.compilers.fsm.variables;

/**
 * Accumulate variable name characters.<br>
 */
class VariableContext {

	private final StringBuilder name = new StringBuilder(15);

	void addNameCharacter(Character character) {

		this.name.append(character);
	}

	public String getName() {

		return this.name.toString();
	}
}
