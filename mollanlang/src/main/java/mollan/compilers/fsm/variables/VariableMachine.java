package mollan.compilers.fsm.variables;

import mollan.compilers.fsm.FiniteStateMachine;
import mollan.compilers.fsm.SingleCharacterState;
import mollan.compilers.fsm.SingleCharacterStateBuilder;
import mollan.compilers.fsm.State;

/**
 * Implementation of {@link FiniteStateMachine} that is used for parse variables in math expressions.<br>
 * See {@link VariableCompiler}.<br>
 * Math expressions: {@link mollan.compilers.fsm.mathexpression.MathExpressionCompiler}.<br>
 */
public class VariableMachine extends FiniteStateMachine<VariableContext> {

	public static VariableMachine create() {

		SingleCharacterState<VariableContext> letterState = new SingleCharacterStateBuilder<VariableContext>()
			.setCharacterPredicate(Character::isLetter)
			.setResultAdapter(VariableContext::addNameCharacter)
			.makeFinish()
			.build();

		letterState.addTransition(letterState);

		return new VariableMachine(letterState);
	}

	@SafeVarargs
	private VariableMachine(State<VariableContext>... states) { super(states); }
}
