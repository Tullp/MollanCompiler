/**
 * See {@link mollan.compilers.fsm.variables.InitVariableCompiler} and {@link mollan.compilers.fsm.variables.VariableCompiler}.<br>
 */
package mollan.compilers.fsm.variables;