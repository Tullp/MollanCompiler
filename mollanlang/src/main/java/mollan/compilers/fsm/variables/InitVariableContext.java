package mollan.compilers.fsm.variables;

import mollan.runtime.Command;

/**
 * Accumulate name of new variable and it value.<br>
 */
class InitVariableContext {

	private final StringBuilder name = new StringBuilder(15);
	private Command valueCommand;

	void addNameCharacter(Character character) {

		this.name.append(character);
	}

	void setValueCommand(Command valueCommand) {

		this.valueCommand = valueCommand;
	}

	public Command getCommand() {

		return runtimeEnvironment -> {

			runtimeEnvironment.openSortingStation();

			this.valueCommand.execute(runtimeEnvironment);

			runtimeEnvironment.setVariable(
				this.name.toString(),
				runtimeEnvironment.getSortingStation().getResult()
			);

			runtimeEnvironment.closeSortingStation();
		};
	}
}
