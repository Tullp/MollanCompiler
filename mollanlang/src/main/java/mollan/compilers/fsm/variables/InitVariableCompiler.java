package mollan.compilers.fsm.variables;

import mollan.compilers.Compiler;
import mollan.compilers.CompilerFactory;
import mollan.compilers.InputChain;
import mollan.compilers.MollanCompileException;
import mollan.runtime.Command;

import java.util.Optional;

/**
 * Implementation of {@link Compiler}, that compiles variables initialization.<br>
 * It's consist of variable name, equals symbol and variable value (expression).<br>
 * More about expressions: {@link mollan.compilers.fsm.expression.ExpressionCompiler}.<br>
 * The compiler is based on {@link InitVariableMachine}.<br>
 */
public class InitVariableCompiler extends Compiler {

	public InitVariableCompiler(CompilerFactory compilerFactory) { super(compilerFactory); }

	@Override
	public Optional<Command> compile(InputChain inputChain) throws MollanCompileException {

		InitVariableContext initVariableContext = new InitVariableContext();

		if (InitVariableMachine.create(compilerFactory).execute(inputChain, initVariableContext, false)) {

			return Optional.of(initVariableContext.getCommand());
		}

		return Optional.empty();
	}
}
