package mollan.compilers.fsm.variables;

import mollan.compilers.Compiler;
import mollan.compilers.CompilerFactory;
import mollan.compilers.InputChain;
import mollan.compilers.MollanCompileException;
import mollan.runtime.Command;
import mollan.runtime.types.ValueHolder;

import java.util.Optional;

/**
 * Implementation of {@link Compiler}, that compiles variables into math expressions.<br>
 * The compiler is based on {@link VariableMachine}.<br>
 */
public class VariableCompiler extends Compiler {

	public VariableCompiler(CompilerFactory compilerFactory) { super(compilerFactory); }

	@Override
	public Optional<Command> compile(InputChain inputChain) throws MollanCompileException {

		VariableContext variableContext = new VariableContext();

		if (VariableMachine.create().execute(inputChain, variableContext, false)) {

			return Optional.of(runtimeEnvironment -> {

				Optional<ValueHolder<?>> value = runtimeEnvironment.getVariable(variableContext.getName());

				if (!value.isPresent()) {

					throw new RuntimeException(
						"Variable with name '" + variableContext.getName() + "' not found"
					);
				}

				runtimeEnvironment.getSortingStation().pushOperand(value.get());
			});
		}

		return Optional.empty();
	}
}
