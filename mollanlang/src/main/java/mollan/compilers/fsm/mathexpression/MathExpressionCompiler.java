package mollan.compilers.fsm.mathexpression;

import mollan.compilers.Compiler;
import mollan.compilers.CompilerFactory;
import mollan.compilers.InputChain;
import mollan.compilers.MollanCompileException;
import mollan.runtime.Command;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Implementation of {@link Compiler}, that compiles math expressions.<br>
 * Math expression is consistent combination of math calculable elements and math operators.<br>
 * Math expression must end with math calculable element.<br>
 * More about math calculable elements: {@link mollan.compilers.fsm.mathcalculable.MathCalculableCompiler}.<br>
 * More about math operators: {@link mollan.runtime.operators.OperatorFactory}.<br>
 * The compiler is based on {@link MathExpressionMachine}.<br>
 */
public class MathExpressionCompiler extends Compiler {

	public MathExpressionCompiler(CompilerFactory compilerFactory) { super(compilerFactory); }

	@Override
	public Optional<Command> compile(InputChain inputChain) throws MollanCompileException {

		List<Command> commands = new ArrayList<>();

		if (MathExpressionMachine.create(compilerFactory).execute(inputChain, commands)) {

			return Optional.of(runtimeEnvironment -> {

				for (Command command : commands) {

					command.execute(runtimeEnvironment);
				}
			});
		}

		return Optional.empty();
	}
}
