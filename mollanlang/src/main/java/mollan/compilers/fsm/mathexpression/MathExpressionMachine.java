package mollan.compilers.fsm.mathexpression;

import mollan.compilers.CompilerFactory;
import mollan.compilers.CompilerType;
import mollan.compilers.fsm.CompilerWrapperState;
import mollan.compilers.fsm.CompilerWrapperStateBuilder;
import mollan.compilers.fsm.FiniteStateMachine;
import mollan.compilers.fsm.GrammaticalWordsState;
import mollan.compilers.fsm.GrammaticalWordsStateBuilder;
import mollan.compilers.fsm.State;
import mollan.runtime.Command;
import mollan.runtime.operators.OperatorFactory;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Implementation of {@link FiniteStateMachine} that is used for parse math expressions.<br>
 * See {@link MathExpressionCompiler}.<br>
 */
public class MathExpressionMachine extends FiniteStateMachine<List<Command>> {

	public static MathExpressionMachine create(CompilerFactory compilerFactory) {

		CompilerWrapperState<List<Command>> calculableState = new CompilerWrapperStateBuilder<List<Command>>(compilerFactory)
			.setCompilerType(CompilerType.MATHCALCULABLE)
			.setResultAdapter(List::add)
			.makeFinish()
			.build();

		GrammaticalWordsState<List<Command>> operatorState = new GrammaticalWordsStateBuilder<List<Command>>()
			.setWords(new OperatorFactory().getMathOperators()
				.stream()
				.sorted((s1, s2) -> s2.length() - s1.length())
				.collect(Collectors.toList())
			).setConsumer((commands, symbol) -> commands.add(runtimeEnvironment ->
				runtimeEnvironment.getSortingStation().pushOperator(
					new OperatorFactory().getMathOperator(symbol)
				)
			)).build();

		calculableState.addTransition(operatorState);
		operatorState.addTransition(calculableState);

		return new MathExpressionMachine(calculableState);
	}

	@SafeVarargs
	private MathExpressionMachine(State<List<Command>>... states) { super(states); }
}
