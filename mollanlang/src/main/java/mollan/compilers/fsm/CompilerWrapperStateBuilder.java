package mollan.compilers.fsm;

import mollan.compilers.CompilerFactory;
import mollan.compilers.CompilerType;
import mollan.runtime.Command;

/**
 * Builder for {@link CompilerWrapperState}.<br>
 *
 * @param <O> type of output chain in terms of finite state machine.<br>
 */
public class CompilerWrapperStateBuilder<O> {

	private final CompilerFactory compilerFactory;
	private boolean isFinish;
	private CompilerType compilerType;
	private ResultAdapter<O, Command> resultAdapter;

	public CompilerWrapperStateBuilder(CompilerFactory compilerFactory) {

		this.compilerFactory = compilerFactory;
	}

	public CompilerWrapperStateBuilder<O> makeFinish() {

		this.isFinish = true;

		return this;
	}

	public CompilerWrapperStateBuilder<O> setCompilerType(CompilerType compilerType) {

		this.compilerType = compilerType;

		return this;
	}

	public CompilerWrapperStateBuilder<O> setResultAdapter(ResultAdapter<O, Command> resultAdapter) {

		this.resultAdapter = resultAdapter;

		return this;
	}

	public CompilerWrapperState<O> build() {

		return new CompilerWrapperState<>(compilerFactory, compilerType, resultAdapter, isFinish);
	}
}
