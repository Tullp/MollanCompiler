package mollan.compilers.fsm;

import mollan.compilers.CompilerFactory;
import mollan.compilers.CompilerType;
import mollan.compilers.InputChain;
import mollan.compilers.MollanCompileException;
import mollan.runtime.Command;

import java.util.Optional;

/**
 * State for call compilers inside a state.<br>
 *
 * @param <O> type of output chain in terms of finite state machine.<br>
 */
public class CompilerWrapperState<O> extends State<O> {

	private final CompilerFactory compilerFactory;
	private final CompilerType compilerType;
	private final ResultAdapter<O, Command> resultAdapter;

	CompilerWrapperState(
			CompilerFactory compilerFactory,
			CompilerType compilerType,
			ResultAdapter<O, Command> resultAdapter,
			boolean isFinish) {

		super(isFinish);

		this.compilerFactory = compilerFactory;

		this.compilerType = compilerType;

		this.resultAdapter = resultAdapter;
	}

	@Override
	public boolean tryTransition(InputChain inputChain, O outputChain) throws MollanCompileException {

		Optional<Command> command = compilerFactory.create(compilerType).compile(inputChain);

		if (command.isPresent()) {

			resultAdapter.adapt(outputChain, command.get());

			return true;
		}

		return false;
	}
}
