package mollan.compilers.fsm.program;

import mollan.compilers.Compiler;
import mollan.compilers.CompilerFactory;
import mollan.compilers.InputChain;
import mollan.compilers.MollanCompileException;
import mollan.runtime.Command;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Implementation of {@link Compiler}, that compiles program.<br>
 * Program is block with one or more statements.<br>
 * More about statements: {@link mollan.compilers.fsm.statement.StatementCompiler}.<br>
 * The compiler is based on {@link ProgramMachine}.<br>
 */
public class ProgramCompiler extends Compiler {

	public ProgramCompiler(CompilerFactory compilerFactory) { super(compilerFactory); }

	@Override
	public Optional<Command> compile(InputChain inputChain) throws MollanCompileException {

		List<Command> commands = new ArrayList<>();

		if (ProgramMachine.create(compilerFactory).execute(inputChain, commands)) {

			return Optional.of(runtimeEnvironment -> {

				for (Command command : commands) {

					command.execute(runtimeEnvironment);
				}
			});
		}

		return Optional.empty();
	}
}
