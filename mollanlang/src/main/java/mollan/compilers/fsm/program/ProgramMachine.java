package mollan.compilers.fsm.program;

import mollan.compilers.CompilerFactory;
import mollan.compilers.CompilerType;
import mollan.compilers.fsm.CompilerWrapperState;
import mollan.compilers.fsm.CompilerWrapperStateBuilder;
import mollan.compilers.fsm.FiniteStateMachine;
import mollan.compilers.fsm.SingleCharacterState;
import mollan.compilers.fsm.SingleCharacterStateBuilder;
import mollan.compilers.fsm.State;
import mollan.runtime.Command;

import java.util.List;

/**
 * Implementation of {@link FiniteStateMachine} that is used for parse program - one or more statements.<br>
 * See {@link ProgramCompiler}.<br>
 */
public class ProgramMachine extends FiniteStateMachine<List<Command>> {

	public static ProgramMachine create(CompilerFactory compilerFactory) {

		SingleCharacterState<List<Command>> semicolonState = new SingleCharacterStateBuilder<List<Command>>()
			.setCharacterPredicate(character -> character == ';')
			.makeFinish()
			.build();

		CompilerWrapperState<List<Command>> statementState = new CompilerWrapperStateBuilder<List<Command>>(compilerFactory)
			.setCompilerType(CompilerType.STATEMENT)
			.setResultAdapter(List::add)
			.build();

		statementState.addTransition(semicolonState);
		semicolonState.addTransition(statementState);

		return new ProgramMachine(statementState);
	}

	@SafeVarargs
	private ProgramMachine(State<List<Command>>... states) { super(states); }
}
