package mollan.compilers.fsm.mathcalculable;

import mollan.runtime.Command;

/**
 * Output chain for MathCalculableMachine.<br>
 * Contains only one command, that calculate this element and push it to sorting station.<br>
 */
class MathCalculableContext {

	private Command value;

	void set(Command value) {
		this.value = value;
	}

	public Command get() {
		return this.value;
	}
}
