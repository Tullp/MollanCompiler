package mollan.compilers.fsm.mathcalculable;

import mollan.compilers.CompilerFactory;
import mollan.compilers.CompilerType;
import mollan.compilers.fsm.CompilerWrapperState;
import mollan.compilers.fsm.CompilerWrapperStateBuilder;
import mollan.compilers.fsm.FiniteStateMachine;
import mollan.compilers.fsm.SingleCharacterState;
import mollan.compilers.fsm.SingleCharacterStateBuilder;
import mollan.compilers.fsm.State;
import mollan.runtime.types.ValueHolder;

/**
 * Implementation of {@link FiniteStateMachine} that is used for parse every calculable element of math expression.<br>
 * See {@link MathCalculableCompiler}.<br>
 */
public class MathCalculableMachine extends FiniteStateMachine<MathCalculableContext> {

	public static MathCalculableMachine create(CompilerFactory compilerFactory) {

		SingleCharacterState<MathCalculableContext> openBracketState = new SingleCharacterStateBuilder<MathCalculableContext>()
			.setCharacterPredicate(character -> character == '(')
			.build();

		SingleCharacterState<MathCalculableContext> closeBracketState = new SingleCharacterStateBuilder<MathCalculableContext>()
			.setCharacterPredicate(character -> character == ')')
			.makeFinish()
			.build();

		CompilerWrapperState<MathCalculableContext> mathExpressionState = new CompilerWrapperStateBuilder<MathCalculableContext>(compilerFactory)
			.setCompilerType(CompilerType.MATHEXPRESSION)
			.setResultAdapter((outputChain, command) -> outputChain.set(runtimeEnvironment -> {

					runtimeEnvironment.openSortingStation();

					command.execute(runtimeEnvironment);

					ValueHolder<?> result = runtimeEnvironment.getSortingStation().getResult();

					runtimeEnvironment.closeSortingStation();

					runtimeEnvironment.getSortingStation().pushOperand(result);
				})
			)
			.build();

		CompilerWrapperState<MathCalculableContext> numberState = new CompilerWrapperStateBuilder<MathCalculableContext>(compilerFactory)
			.setCompilerType(CompilerType.NUMBER)
			.setResultAdapter(MathCalculableContext::set)
			.makeFinish()
			.build();

		CompilerWrapperState<MathCalculableContext> functionState = new CompilerWrapperStateBuilder<MathCalculableContext>(compilerFactory)
			.setCompilerType(CompilerType.FUNCTION)
			.setResultAdapter(MathCalculableContext::set)
			.makeFinish()
			.build();

		CompilerWrapperState<MathCalculableContext> variableState = new CompilerWrapperStateBuilder<MathCalculableContext>(compilerFactory)
			.setCompilerType(CompilerType.VARIABLE)
			.setResultAdapter(MathCalculableContext::set)
			.makeFinish()
			.build();

		openBracketState.addTransition(mathExpressionState);
		mathExpressionState.addTransition(closeBracketState);

		return new MathCalculableMachine(
			openBracketState,
			numberState,
			functionState,
			variableState
		);
	}

	@SafeVarargs
	private MathCalculableMachine(State<MathCalculableContext>... states) { super(states); }
}
