package mollan.compilers.fsm.mathcalculable;

import mollan.compilers.Compiler;
import mollan.compilers.CompilerFactory;
import mollan.compilers.InputChain;
import mollan.compilers.MollanCompileException;
import mollan.runtime.Command;

import java.util.Optional;

/**
 * Implementation of {@link Compiler}, that compiles calculable elements of math expressions.<br>
 * At example: number, variable, math function, brackets, that contains another math expression.<br>
 * The compiler is based on {@link MathCalculableMachine}.<br>
 */
public class MathCalculableCompiler extends Compiler {

	public MathCalculableCompiler(CompilerFactory compilerFactory) { super(compilerFactory); }

	@Override
	public Optional<Command> compile(InputChain inputChain) throws MollanCompileException {

		MathCalculableContext mathCalculableContext = new MathCalculableContext();

		if (MathCalculableMachine.create(compilerFactory).execute(inputChain, mathCalculableContext)) {

			return Optional.of(mathCalculableContext.get());
		}

		return Optional.empty();
	}
}
