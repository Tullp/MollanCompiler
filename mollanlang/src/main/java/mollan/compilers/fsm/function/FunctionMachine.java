package mollan.compilers.fsm.function;

import mollan.compilers.CompilerFactory;
import mollan.compilers.CompilerType;
import mollan.compilers.fsm.CompilerWrapperState;
import mollan.compilers.fsm.CompilerWrapperStateBuilder;
import mollan.compilers.fsm.FiniteStateMachine;
import mollan.compilers.fsm.SingleCharacterState;
import mollan.compilers.fsm.SingleCharacterStateBuilder;
import mollan.compilers.fsm.State;

/**
 * Implementation of {@link FiniteStateMachine} that is used for parse math functions.<br>
 * See {@link FunctionCompiler}.<br>
 */
public class FunctionMachine extends FiniteStateMachine<FunctionContext> {

	public static FunctionMachine create(CompilerFactory compilerFactory) {

		SingleCharacterState<FunctionContext> letterState = new SingleCharacterStateBuilder<FunctionContext>()
			.setCharacterPredicate(Character::isLetter)
			.setResultAdapter(FunctionContext::addNameCharacter)
			.build();

		SingleCharacterState<FunctionContext> digitState = new SingleCharacterStateBuilder<FunctionContext>()
			.setCharacterPredicate(Character::isDigit)
			.setResultAdapter(FunctionContext::addNameCharacter)
			.build();

		CompilerWrapperState<FunctionContext> argumentState = new CompilerWrapperStateBuilder<FunctionContext>(compilerFactory)
			.setCompilerType(CompilerType.MATHEXPRESSION)
			.setResultAdapter(FunctionContext::addArgumentCommand)
			.build();

		SingleCharacterState<FunctionContext> openBracketState = new SingleCharacterStateBuilder<FunctionContext>()
			.setCharacterPredicate(character -> character == '(')
			.build();

		SingleCharacterState<FunctionContext> closeBracketState = new SingleCharacterStateBuilder<FunctionContext>()
			.setCharacterPredicate(character -> character == ')')
			.makeFinish()
			.build();

		SingleCharacterState<FunctionContext> commaState = new SingleCharacterStateBuilder<FunctionContext>()
			.setCharacterPredicate(character -> character == ',')
			.build();

		letterState.addTransition(letterState, digitState, openBracketState);
		digitState.addTransition(digitState, letterState, openBracketState);
		openBracketState.addTransition(argumentState, closeBracketState);
		argumentState.addTransition(commaState, closeBracketState);
		commaState.addTransition(argumentState);

		return new FunctionMachine(letterState);
	}

	@SafeVarargs
	private FunctionMachine(State<FunctionContext>... states) { super(states); }
}
