package mollan.compilers.fsm.function;

import mollan.runtime.Command;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/**
 * Output chain for function. Generate command for calculate function, her arguments and push result to SoringStation.<br>
 */
class FunctionContext {

	private final StringBuilder functionName = new StringBuilder(20);
	private final Collection<Command> functionArgumentsCommands = new ArrayList<>();

	void addNameCharacter(Character character) {
		this.functionName.append(character);
	}

	void addArgumentCommand(Command argumentCommand) {

		this.functionArgumentsCommands.add(argumentCommand);
	}

	public String getFunctionName() {

		return this.functionName.toString();
	}

	public Collection<Command> getFunctionArgumentsCommands() {

		return Collections.unmodifiableCollection(functionArgumentsCommands);
	}
}
