package mollan.compilers.fsm.function;

import mollan.compilers.Compiler;
import mollan.compilers.CompilerFactory;
import mollan.compilers.InputChain;
import mollan.compilers.MollanCompileException;
import mollan.runtime.Command;
import mollan.runtime.functions.FunctionFactory;
import mollan.runtime.functions.MathFunction;
import mollan.runtime.types.doubletype.DoubleValueHolder;
import mollan.runtime.types.doubletype.DoubleValueVisitor;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Implementation of {@link Compiler}, that compiles math functions.<br>
 * Math function: {@link MathFunction}.<br>
 * Math functions list: {@link FunctionFactory}.<br>
 * The compiler is based on {@link FunctionMachine}.<br>
 */
public class FunctionCompiler extends Compiler {

	public FunctionCompiler(CompilerFactory compilerFactory) { super(compilerFactory); }

	@Override
	public Optional<Command> compile(InputChain inputChain) throws MollanCompileException {

		FunctionContext functionContext = new FunctionContext();

		if (FunctionMachine.create(compilerFactory).execute(inputChain, functionContext, false)) {

			Optional<MathFunction> mathFunction = new FunctionFactory().getFunction(functionContext.getFunctionName());

			if (!mathFunction.isPresent()) {

				throw new MollanCompileException(
					String.format("Math function with name '%s' doesn't exist.", functionContext.getFunctionName()),
					inputChain.getPointer()
				);
			}

			if (!mathFunction.get().checkArgumentsCount(functionContext.getFunctionArgumentsCommands().size())) {

				throw new MollanCompileException(
					String.format("Math function with name '%s' doesn't take %d arguments.",
						functionContext.getFunctionName(), functionContext.getFunctionArgumentsCommands().size()),
					inputChain.getPointer()
				);
			}

			return Optional.of(runtimeEnvironment -> {

				List<Double> arguments = new ArrayList<>();

				for (Command command : functionContext.getFunctionArgumentsCommands()) {

					runtimeEnvironment.openSortingStation();

					command.execute(runtimeEnvironment);

					Optional<Double> result = DoubleValueVisitor.getDoubleValue(
						runtimeEnvironment.getSortingStation().getResult()
					);

					if (!result.isPresent()) {

						// non-existent in case of correct grammar.
						throw new RuntimeException("MathFunction supports only double.");
					}

					arguments.add(result.get());

					runtimeEnvironment.closeSortingStation();
				}

				runtimeEnvironment.getSortingStation().pushOperand(
					new DoubleValueHolder(mathFunction.get().calculate(arguments))
				);
			});
		}

		return Optional.empty();
	}
}
