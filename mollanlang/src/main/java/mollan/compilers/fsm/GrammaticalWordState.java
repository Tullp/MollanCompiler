package mollan.compilers.fsm;

import mollan.compilers.InputChain;

import java.util.function.Consumer;

public class GrammaticalWordState<O> extends State<O> {

	private final String word;
	private final Consumer<O> consumer;

	GrammaticalWordState(String word, Consumer<O> consumer, boolean isFinish) {

		super(isFinish);
		this.word = word;
		this.consumer = consumer;
	}

	@Override
	public boolean tryTransition(InputChain inputChain, O outputChain) {

		if (inputChain.getPart(inputChain.getPointer(), this.word.length()).equals(this.word)) {

			inputChain.increasePointer(this.word.length());

			this.consumer.accept(outputChain);

			return true;
		}

		return false;
	}
}
