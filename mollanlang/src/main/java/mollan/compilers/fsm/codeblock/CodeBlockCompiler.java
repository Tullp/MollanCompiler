package mollan.compilers.fsm.codeblock;

import mollan.compilers.Compiler;
import mollan.compilers.CompilerFactory;
import mollan.compilers.InputChain;
import mollan.compilers.MollanCompileException;
import mollan.runtime.Command;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Implementation of {@link Compiler}, that compiles blocks of code.<br>
 * Block of code is program between braces. More about program: {@link mollan.compilers.fsm.program.ProgramCompiler}.<br>
 * The compiler is based on {@link CodeBlockMachine}.<br>
 */
public class CodeBlockCompiler extends Compiler {

	public CodeBlockCompiler(CompilerFactory compilerFactory) { super(compilerFactory); }

	@Override
	public Optional<Command> compile(InputChain inputChain) throws MollanCompileException {

		List<Command> commands = new ArrayList<>();

		if (CodeBlockMachine.create(compilerFactory).execute(inputChain, commands)) {

			return Optional.of(runtimeEnvironment -> {

				for (Command command : commands) {

					command.execute(runtimeEnvironment);
				}
			});
		}

		return Optional.empty();
	}
}
