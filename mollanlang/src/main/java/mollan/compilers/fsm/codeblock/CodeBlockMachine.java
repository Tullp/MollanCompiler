package mollan.compilers.fsm.codeblock;

import mollan.compilers.CompilerFactory;
import mollan.compilers.CompilerType;
import mollan.compilers.fsm.CompilerWrapperStateBuilder;
import mollan.compilers.fsm.FiniteStateMachine;
import mollan.compilers.fsm.GrammaticalWordStateBuilder;
import mollan.compilers.fsm.State;
import mollan.runtime.Command;

import java.util.List;

/**
 * Implementation of {@link FiniteStateMachine} that is used for parse blocks of code.<br>
 * See {@link CodeBlockMachine}.<br>
 */
public class CodeBlockMachine extends FiniteStateMachine<List<Command>> {

	@SafeVarargs
	private CodeBlockMachine(State<List<Command>>... startStates) {
		super(startStates);
	}

	public static CodeBlockMachine create(CompilerFactory compilerFactory) {

		State<List<Command>> codeState = new CompilerWrapperStateBuilder<List<Command>>(compilerFactory)
			.setCompilerType(CompilerType.PROGRAM)
			.setResultAdapter(List::add)
			.build();

		State<List<Command>> openBraceState = new GrammaticalWordStateBuilder<List<Command>>()
			.setWord("{")
			.build();

		State<List<Command>> closeBraceState = new GrammaticalWordStateBuilder<List<Command>>()
			.setWord("}")
			.makeFinish()
			.build();

		openBraceState.addTransition(codeState, closeBraceState);
		codeState.addTransition(closeBraceState);

		return new CodeBlockMachine(openBraceState);
	}
}
