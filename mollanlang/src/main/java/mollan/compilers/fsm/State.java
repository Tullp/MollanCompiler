package mollan.compilers.fsm;

import mollan.compilers.InputChain;
import mollan.compilers.MollanCompileException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Node, that contains in machines. Fills output chain and increase InputChain pointer.<br>
 * @param <O> output chain from machine, that contains this state.<br>
 */
public abstract class State<O> {

	private final boolean isFinish;

	private final List<State<O>> transitions = new ArrayList<>();

	State(boolean isFinish) {
		this.isFinish = isFinish;
	}

	boolean isFinish() { return this.isFinish; }

	List<State<O>> getTransitions() {

		return Collections.unmodifiableList(this.transitions);
	}

	@SafeVarargs
	public final void addTransition(State<O>... transitions) {
		this.transitions.addAll(Arrays.asList(transitions));
	}

	public abstract boolean tryTransition(InputChain inputChain, O outputChain) throws MollanCompileException;
}
