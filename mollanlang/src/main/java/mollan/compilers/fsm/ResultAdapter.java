package mollan.compilers.fsm;

/**
 * Adapter, that adapt state processing result to state output.<br>
 *
 * @param <O> type of output chain in terms of finite state machine.<br>
 * @param <R> state result, at example: in SingleCharacterState - Character, in CompilerWrapperState - Command.<br>
 */
@FunctionalInterface
public interface ResultAdapter<O, R> {

	void adapt(O outputChain, R result);
}
