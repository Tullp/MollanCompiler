package mollan.compilers.fsm.switchoperator;

import mollan.runtime.Command;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Output chain for {@link SwitchMachine}.<br>
 */
class SwitchContext {

	private Command variableCommand;
	private final List<Command> numberCaseCommands = new ArrayList<>();
	private final List<Command> caseBodyCommands = new ArrayList<>();
	private Command defaultCommand = runtimeEnvironment -> {};

	void setVariableCommand(Command variableCommand) {

		this.variableCommand = variableCommand;
	}

	Command getVariableCommand() {

		return variableCommand;
	}

	void addNumberCase(Command command) {

		numberCaseCommands.add(command);
	}

	List<Command> getNumberCaseCommands() {

		return Collections.unmodifiableList(numberCaseCommands);
	}

	void addCaseBodyCommand(Command command) {

		caseBodyCommands.add(command);
	}

	List<Command> getCaseBodyCommands() {

		return Collections.unmodifiableList(caseBodyCommands);
	}

	void setDefaultCommand(Command defaultCommand) {

		this.defaultCommand = defaultCommand;
	}

	Command getDefaultCommand() {

		return defaultCommand;
	}
}
