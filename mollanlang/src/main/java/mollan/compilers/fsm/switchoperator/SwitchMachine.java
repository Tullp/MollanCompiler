package mollan.compilers.fsm.switchoperator;

import mollan.compilers.CompilerFactory;
import mollan.compilers.CompilerType;
import mollan.compilers.fsm.CompilerWrapperStateBuilder;
import mollan.compilers.fsm.FiniteStateMachine;
import mollan.compilers.fsm.GrammaticalWordStateBuilder;
import mollan.compilers.fsm.SingleCharacterStateBuilder;
import mollan.compilers.fsm.State;

/**
 * Implementation of {@link FiniteStateMachine} that is used for parse 'switch' statements.<br>
 * See {@link SwitchCompiler}.<br>
 */
public class SwitchMachine extends FiniteStateMachine<SwitchContext> {

	@SafeVarargs
	private SwitchMachine(State<SwitchContext>... startStates) {
		super(startStates);
	}

	public static SwitchMachine create(CompilerFactory compilerFactory) {

		State<SwitchContext> switchWordState = new GrammaticalWordStateBuilder<SwitchContext>()
			.setWord("switch")
			.build();

		State<SwitchContext> openVariableBracketState = new SingleCharacterStateBuilder<SwitchContext>()
			.setCharacterPredicate(character -> character.equals('('))
			.build();

		State<SwitchContext> variableState = new CompilerWrapperStateBuilder<SwitchContext>(compilerFactory)
			.setCompilerType(CompilerType.VARIABLE)
			.setResultAdapter(SwitchContext::setVariableCommand)
			.build();

		State<SwitchContext> closeVariableBracketState = new SingleCharacterStateBuilder<SwitchContext>()
			.setCharacterPredicate(character -> character.equals(')'))
			.build();

		State<SwitchContext> openSwitchBodyBraceState = new SingleCharacterStateBuilder<SwitchContext>()
			.setCharacterPredicate(character -> character.equals('{'))
			.build();

		State<SwitchContext> closeSwitchBodyBraceState = new SingleCharacterStateBuilder<SwitchContext>()
			.setCharacterPredicate(character -> character.equals('}'))
			.makeFinish()
			.build();

		State<SwitchContext> caseWordState = new GrammaticalWordStateBuilder<SwitchContext>()
			.setWord("case")
			.build();

		State<SwitchContext> numberCaseState = new CompilerWrapperStateBuilder<SwitchContext>(compilerFactory)
			.setCompilerType(CompilerType.NUMBER)
			.setResultAdapter(SwitchContext::addNumberCase)
			.build();

		State<SwitchContext> colonState = new SingleCharacterStateBuilder<SwitchContext>()
			.setCharacterPredicate(character -> character.equals(':'))
			.build();

		State<SwitchContext> caseBodyState = new CompilerWrapperStateBuilder<SwitchContext>(compilerFactory)
			.setCompilerType(CompilerType.CODEBLOCK)
			.setResultAdapter(SwitchContext::addCaseBodyCommand)
			.build();

		State<SwitchContext> defaultWordState = new GrammaticalWordStateBuilder<SwitchContext>()
			.setWord("default")
			.build();

		State<SwitchContext> defaultColonState = new SingleCharacterStateBuilder<SwitchContext>()
			.setCharacterPredicate(character -> character.equals(':'))
			.build();

		State<SwitchContext> defaultBodyState = new CompilerWrapperStateBuilder<SwitchContext>(compilerFactory)
			.setCompilerType(CompilerType.CODEBLOCK)
			.setResultAdapter(SwitchContext::setDefaultCommand)
			.build();


		// Head
		switchWordState.addTransition(openVariableBracketState);
		openVariableBracketState.addTransition(variableState);
		variableState.addTransition(closeVariableBracketState);
		closeVariableBracketState.addTransition(openSwitchBodyBraceState);
		openSwitchBodyBraceState.addTransition(caseWordState, closeSwitchBodyBraceState);

		// Case
		caseWordState.addTransition(numberCaseState);
		numberCaseState.addTransition(colonState);
		colonState.addTransition(caseBodyState);
		caseBodyState.addTransition(caseWordState, defaultWordState, closeSwitchBodyBraceState);

		// Default
		defaultWordState.addTransition(defaultColonState);
		defaultColonState.addTransition(defaultBodyState);
		defaultBodyState.addTransition(closeSwitchBodyBraceState);


		return new SwitchMachine(switchWordState);
	}
}
