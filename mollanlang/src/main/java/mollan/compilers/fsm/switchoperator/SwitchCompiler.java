package mollan.compilers.fsm.switchoperator;

import mollan.compilers.Compiler;
import mollan.compilers.CompilerFactory;
import mollan.compilers.InputChain;
import mollan.compilers.MollanCompileException;
import mollan.runtime.Command;
import mollan.runtime.types.ValueHolder;

import java.util.Optional;

/**
 * Implementation of {@link Compiler}, that compiles 'switch' statement.<br>
 * Syntax:
 * switch (variable) {
 *     case number1: codeblock
 *     case number2: codeblock
 *     ...<br>
 *     default: codeblock
 * };
 * More about blocks of code: {@link mollan.compilers.fsm.codeblock.CodeBlockCompiler}.<br>
 * More about 'switch' statement: <a href="https://en.wikipedia.org/wiki/Switch_statement">wiki</a>.<br>
 * The compiler is based on {@link SwitchMachine}.<br>
 */
public class SwitchCompiler extends Compiler {

	public SwitchCompiler(CompilerFactory compilerFactory) { super(compilerFactory); }

	@Override
	public Optional<Command> compile(InputChain inputChain) throws MollanCompileException {

		SwitchContext switchContext = new SwitchContext();

		if (SwitchMachine.create(compilerFactory).execute(inputChain, switchContext)) {

			return Optional.of(runtimeEnvironment -> {

				runtimeEnvironment.openSortingStation();

				switchContext.getVariableCommand().execute(runtimeEnvironment);

				ValueHolder<?> variableValue = runtimeEnvironment.getSortingStation().getResult();

				runtimeEnvironment.closeSortingStation();

				for (int i = 0; i < switchContext.getNumberCaseCommands().size(); i++) {

					runtimeEnvironment.openSortingStation();

					switchContext.getNumberCaseCommands().get(i).execute(runtimeEnvironment);

					ValueHolder<?> caseValue = runtimeEnvironment.getSortingStation().getResult();

					runtimeEnvironment.closeSortingStation();

					if (variableValue.value().equals(caseValue.value())) {

						switchContext.getCaseBodyCommands().get(i).execute(runtimeEnvironment);

						return;
					}
				}

				switchContext.getDefaultCommand().execute(runtimeEnvironment);
			});
		}

		return Optional.empty();
	}
}
