package mollan.compilers.fsm.ifstatement;

import mollan.compilers.Compiler;
import mollan.compilers.CompilerFactory;
import mollan.compilers.InputChain;
import mollan.compilers.MollanCompileException;
import mollan.runtime.Command;
import mollan.runtime.types.booleantype.BooleanValueVisitor;

import java.util.Optional;

/**
 * Implementation of {@link Compiler}, that compiles <a href="https://en.wikipedia.org/wiki/Conditional_(computer_programming)">'if' statement</a>.<br>
 * Mollan`s 'if' statement doesn't support 'else' block.<br>
 * As condition compiles boolean expression or single variable.<br>
 * More about boolean expressions: {@link mollan.compilers.fsm.boolexpression.BoolExpressionCompiler}.<br>
 * More about variables: {@link mollan.compilers.fsm.variables.VariableCompiler}.<br>
 * The compiler is based on {@link IfStatementMachine}.<br>
 */
public class IfStatementCompiler extends Compiler {

	public IfStatementCompiler(CompilerFactory compilerFactory) { super(compilerFactory); }

	@Override
	public Optional<Command> compile(InputChain inputChain) throws MollanCompileException {

		IfStatementContext ifStatementContext = new IfStatementContext();

		if (IfStatementMachine.create(compilerFactory).execute(inputChain, ifStatementContext)) {

			return Optional.of(runtimeEnvironment -> {

				runtimeEnvironment.openSortingStation();

				ifStatementContext.getCondition().execute(runtimeEnvironment);

				Optional<Boolean> conditionResult = BooleanValueVisitor.getBooleanValue(
					runtimeEnvironment.getSortingStation().getResult()
				);

				runtimeEnvironment.closeSortingStation();

				if (!conditionResult.isPresent()) {

					throw new RuntimeException("If condition must be boolean.");
				}

				if (conditionResult.get()) {

					ifStatementContext.getBody().execute(runtimeEnvironment);
				}
			});
		}

		return Optional.empty();
	}
}
