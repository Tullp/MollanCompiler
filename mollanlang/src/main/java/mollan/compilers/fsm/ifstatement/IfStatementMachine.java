package mollan.compilers.fsm.ifstatement;

import mollan.compilers.CompilerFactory;
import mollan.compilers.CompilerType;
import mollan.compilers.fsm.CompilerWrapperState;
import mollan.compilers.fsm.CompilerWrapperStateBuilder;
import mollan.compilers.fsm.FiniteStateMachine;
import mollan.compilers.fsm.GrammaticalWordStateBuilder;
import mollan.compilers.fsm.State;

/**
 * Implementation of {@link FiniteStateMachine} that is used for parse 'if' statement.<br>
 * See {@link IfStatementCompiler}.<br>
 */
public class IfStatementMachine extends FiniteStateMachine<IfStatementContext> {

	@SafeVarargs
	private IfStatementMachine(State<IfStatementContext>... startStates) {
		super(startStates);
	}

	public static IfStatementMachine create(CompilerFactory compilerFactory) {

		State<IfStatementContext> ifWordState = new GrammaticalWordStateBuilder<IfStatementContext>()
			.setWord("if")
			.build();

		State<IfStatementContext> openBracketState = new GrammaticalWordStateBuilder<IfStatementContext>()
			.setWord("(")
			.build();

		State<IfStatementContext> conditionState = new CompilerWrapperStateBuilder<IfStatementContext>(compilerFactory)
			.setCompilerType(CompilerType.BOOLEXPRESSION)
			.setResultAdapter(IfStatementContext::setCondition)
			.build();

		CompilerWrapperState<IfStatementContext> variableState = new CompilerWrapperStateBuilder<IfStatementContext>(compilerFactory)
			.setCompilerType(CompilerType.VARIABLE)
			.setResultAdapter(IfStatementContext::setCondition)
			.build();

		State<IfStatementContext> closeBracketState = new GrammaticalWordStateBuilder<IfStatementContext>()
			.setWord(")")
			.build();

		State<IfStatementContext> bodyState = new CompilerWrapperStateBuilder<IfStatementContext>(compilerFactory)
			.setCompilerType(CompilerType.CODEBLOCK)
			.setResultAdapter(IfStatementContext::setBody)
			.makeFinish()
			.build();

		ifWordState.addTransition(openBracketState);
		openBracketState.addTransition(conditionState, variableState);
		conditionState.addTransition(closeBracketState);
		variableState.addTransition(closeBracketState);
		closeBracketState.addTransition(bodyState);

		return new IfStatementMachine(ifWordState);
	}
}
