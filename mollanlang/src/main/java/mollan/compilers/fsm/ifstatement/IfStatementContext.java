package mollan.compilers.fsm.ifstatement;

import mollan.runtime.Command;

/**
 * Output chain for {@link IfStatementMachine}.<br>
 */
public class IfStatementContext {

	private Command condition;

	private Command body;

	public Command getCondition() {

		return condition;
	}

	public void setCondition(Command condition) {

		this.condition = condition;
	}

	public Command getBody() {

		return body;
	}

	public void setBody(Command body) {

		this.body = body;
	}
}
