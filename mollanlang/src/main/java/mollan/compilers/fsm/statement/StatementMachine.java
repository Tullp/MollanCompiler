package mollan.compilers.fsm.statement;

import mollan.compilers.CompilerFactory;
import mollan.compilers.CompilerType;
import mollan.compilers.fsm.CompilerWrapperStateBuilder;
import mollan.compilers.fsm.FiniteStateMachine;
import mollan.compilers.fsm.State;

/**
 * Implementation of {@link FiniteStateMachine} that is used for parse statements.<br>
 * See {@link StatementCompiler}.<br>
 */
public class StatementMachine extends FiniteStateMachine<StatementContext> {

	public static StatementMachine create(CompilerFactory compilerFactory) {

		State<StatementContext> initVariableState = new CompilerWrapperStateBuilder<StatementContext>(compilerFactory)
			.setCompilerType(CompilerType.INITVARIABLE)
			.setResultAdapter(StatementContext::set)
			.makeFinish()
			.build();

		State<StatementContext> forLoopState = new CompilerWrapperStateBuilder<StatementContext>(compilerFactory)
			.setCompilerType(CompilerType.FORLOOP)
			.setResultAdapter(StatementContext::set)
			.makeFinish()
			.build();

		State<StatementContext> switchState = new CompilerWrapperStateBuilder<StatementContext>(compilerFactory)
			.setCompilerType(CompilerType.SWITCH)
			.setResultAdapter(StatementContext::set)
			.makeFinish()
			.build();

		State<StatementContext> ifStatementState = new CompilerWrapperStateBuilder<StatementContext>(compilerFactory)
			.setCompilerType(CompilerType.IFSTATEMENT)
			.setResultAdapter(StatementContext::set)
			.makeFinish()
			.build();

		State<StatementContext> procedureState = new CompilerWrapperStateBuilder<StatementContext>(compilerFactory)
			.setCompilerType(CompilerType.PROCEDURE)
			.setResultAdapter(StatementContext::set)
			.makeFinish()
			.build();

		return new StatementMachine(initVariableState, ifStatementState, switchState, forLoopState, procedureState);
	}

	@SafeVarargs
	private StatementMachine(State<StatementContext>... states) { super(states); }
}
