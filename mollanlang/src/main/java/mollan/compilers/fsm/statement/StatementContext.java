package mollan.compilers.fsm.statement;

import mollan.runtime.Command;

/**
 * Output chain for StatementMachine.<br>
 * Contains only one command.<br>
 */
class StatementContext {

	private Command value;

	void set(Command value) {
		this.value = value;
	}

	Command get() {
		return this.value;
	}
}
