package mollan.compilers.fsm.statement;

import mollan.compilers.Compiler;
import mollan.compilers.CompilerFactory;
import mollan.compilers.InputChain;
import mollan.compilers.MollanCompileException;
import mollan.runtime.Command;

import java.util.Optional;

/**
 * Implementation of {@link Compiler}, that compiles statements.<br>
 * Statement - part of a program. Statement may be consist of variable initialization,
 * procedure call or any construction like 'for' loop or 'switch' statement.<br>
 * The compiler is based on {@link StatementMachine}.<br>
 */
public class StatementCompiler extends Compiler {

	public StatementCompiler(CompilerFactory compilerFactory) { super(compilerFactory); }

	@Override
	public Optional<Command> compile(InputChain inputChain) throws MollanCompileException {

		StatementContext statementContext = new StatementContext();

		if (StatementMachine.create(compilerFactory).execute(inputChain, statementContext)) {

			return Optional.of(statementContext.get());
		}

		return Optional.empty();
	}
}
