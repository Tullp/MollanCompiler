package mollan.compilers.fsm;

import mollan.compilers.InputChain;

import java.util.function.Predicate;

/**
 * State for capture and process single character.<br>
 *
 * @param <O> type of output chain in terms of finite state machine.<br>
 */
public class SingleCharacterState<O> extends State<O> {

	private final Predicate<Character> characterPredicate;
	private final ResultAdapter<O, Character> resultAdapter;

	SingleCharacterState(
			Predicate<Character> characterPredicate,
			ResultAdapter<O, Character> resultAdapter,
			boolean isFinish) {

		super(isFinish);
		this.characterPredicate = characterPredicate;
		this.resultAdapter = resultAdapter;
	}

	@Override
	public boolean tryTransition(InputChain inputChain, O outputChain) {

		if (characterPredicate.test(inputChain.getCurrent())) {

			resultAdapter.adapt(outputChain, inputChain.getCurrent());

			inputChain.increasePointer(1);

			return true;
		}

		return false;
	}
}
