/**
 * Finite state machine API. <a href="https://en.wikipedia.org/wiki/Finite-state_machine">Wiki.</a>.<br>
 */
package mollan.compilers.fsm;