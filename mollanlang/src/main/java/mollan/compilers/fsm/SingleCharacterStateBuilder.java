package mollan.compilers.fsm;

import java.util.function.Predicate;

/**
 * Builder for {@link SingleCharacterState}.<br>
 *
 * @param <O> type of output chain in terms of finite state machine.<br>
 */
public class SingleCharacterStateBuilder<O> {

	private boolean isFinish = false;
	private Predicate<Character> characterPredicate = character -> false;
	private ResultAdapter<O, Character> resultAdapter = (outputChain, result) -> {};

	public SingleCharacterStateBuilder<O> makeFinish() {

		this.isFinish = true;

		return this;
	}

	public SingleCharacterStateBuilder<O> setCharacterPredicate(Predicate<Character> characterPredicate) {

		this.characterPredicate = characterPredicate;

		return this;
	}

	public SingleCharacterStateBuilder<O> setResultAdapter(ResultAdapter<O, Character> resultAdapter) {

		this.resultAdapter = resultAdapter;

		return this;
	}

	public SingleCharacterState<O> build() {

		return new SingleCharacterState<>(characterPredicate, resultAdapter, isFinish);
	}
}
