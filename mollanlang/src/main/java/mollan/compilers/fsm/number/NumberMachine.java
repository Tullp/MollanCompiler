package mollan.compilers.fsm.number;

import mollan.compilers.fsm.FiniteStateMachine;
import mollan.compilers.fsm.SingleCharacterState;
import mollan.compilers.fsm.SingleCharacterStateBuilder;
import mollan.compilers.fsm.State;

/**
 * Implementation of {@link FiniteStateMachine} that is used for parse numbers.<br>
 * See {@link NumberCompiler}.<br>
 */
public class NumberMachine extends FiniteStateMachine<StringBuilder> {

	public static NumberMachine create() {

		SingleCharacterState<StringBuilder> minusState = new SingleCharacterStateBuilder<StringBuilder>()
			.setCharacterPredicate(character -> character == '-')
			.setResultAdapter(StringBuilder::append)
			.build();

		SingleCharacterState<StringBuilder> dotState = new SingleCharacterStateBuilder<StringBuilder>()
			.setCharacterPredicate(character -> character == '.')
			.setResultAdapter(StringBuilder::append)
			.build();

		SingleCharacterStateBuilder<StringBuilder> digitStateBuilder = new SingleCharacterStateBuilder<StringBuilder>()
			.setCharacterPredicate(Character::isDigit)
			.setResultAdapter(StringBuilder::append)
			.makeFinish();

		SingleCharacterState<StringBuilder> integerPartState = digitStateBuilder.build();

		SingleCharacterState<StringBuilder> decimalPartState = digitStateBuilder.build();

		minusState.addTransition(integerPartState);
		integerPartState.addTransition(integerPartState, dotState);
		dotState.addTransition(decimalPartState);
		decimalPartState.addTransition(decimalPartState);

		return new NumberMachine(minusState, integerPartState);
	}

	@SafeVarargs
	private NumberMachine(State<StringBuilder>... states) { super(states); }
}
