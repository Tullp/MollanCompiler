package mollan.compilers.fsm.number;

import mollan.compilers.Compiler;
import mollan.compilers.CompilerFactory;
import mollan.compilers.InputChain;
import mollan.compilers.MollanCompileException;
import mollan.runtime.Command;
import mollan.runtime.types.doubletype.DoubleValueHolder;

import java.util.Optional;

/**
 * Implementation of {@link Compiler}, that compiles numbers.<br>
 * Numbers may be integer ot double.<br>
 * The compiler is based on {@link NumberMachine}.<br>
 */
public class NumberCompiler extends Compiler {

	public NumberCompiler(CompilerFactory compilerFactory) { super(compilerFactory); }

	@Override
	public Optional<Command> compile(InputChain inputChain) throws MollanCompileException {

		StringBuilder stringBuilder = new StringBuilder(10);

		if(NumberMachine.create().execute(inputChain, stringBuilder)) {

			return Optional.of(runtimeEnvironment -> runtimeEnvironment
				.getSortingStation()
				.pushOperand(new DoubleValueHolder(Double.parseDouble(stringBuilder.toString())))
			);
		}

		return Optional.empty();
	}
}
