package mollan.compilers.fsm;

import java.util.function.Consumer;

public class GrammaticalWordStateBuilder<O> {

	private boolean isFinish = false;
	private String word;
	private Consumer<O> consumer = output -> {};

	public GrammaticalWordStateBuilder<O> makeFinish() {

		this.isFinish = true;

		return this;
	}

	public GrammaticalWordStateBuilder<O> setWord(String word) {

		this.word = word;

		return this;
	}

	public GrammaticalWordStateBuilder<O> setConsumer(Consumer<O> consumer) {

		this.consumer = consumer;

		return this;
	}

	public GrammaticalWordState<O> build() {

		return new GrammaticalWordState<>(word, consumer, isFinish);
	}
}
