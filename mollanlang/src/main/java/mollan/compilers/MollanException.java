package mollan.compilers;

/**
 * This exception should be thrown when there is syntax error in mathexpression.<br>
 */
public class MollanException extends Exception {

	private static final long serialVersionUID = 7257983368239439743L;
	private final int position;

	MollanException(String message, int position) {

		super(message + ": " + position);

		this.position = position;
	}

	public int getPosition() {
		return position;
	}
}
