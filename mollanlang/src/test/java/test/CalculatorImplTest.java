package test;

import mollan.Calculator;
import mollan.compilers.MollanException;
import mollan.impl.CalculatorImpl;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static com.google.common.truth.Truth.assertThat;
import static com.google.common.truth.Truth.assertWithMessage;

@SuppressWarnings("ALL")
class CalculatorImplTest {

	void testCalculatorImpl(double expectedResult, String inputString) throws MollanException {

		Calculator calculator = new CalculatorImpl();

		double actualResult = calculator.calculate(inputString);

		assertWithMessage("test %s. Excpect %s, actual %s", inputString, expectedResult, actualResult)
			.that(actualResult)
			.isEqualTo(expectedResult);
	}

	void testCalculatorImplExceptions(String inputString, int position) {

		try {

			new CalculatorImpl().calculate(inputString);

		} catch (MollanException exception) {

			assertWithMessage("test exception in %s at position %s, get %s (%s)",
					inputString, position, exception.getClass().getSimpleName(), exception.getMessage())
				.that(exception.getPosition())
				.isEqualTo(position);

		} catch (RuntimeException exception) {

			assertThat(-1).isEqualTo(position);
		}
	}


	@ParameterizedTest
	@MethodSource("invalidSyntaxExceptionTests")
	void invalidSyntaxExceptionTest(String inputString, int position) {

		testCalculatorImplExceptions(inputString, position);
	}

	static Stream<Arguments> invalidSyntaxExceptionTests() {

		return Stream.of(
			Arguments.of(".", 0),
			Arguments.of("1.", 2),
			Arguments.of("3 + .", 4),
			Arguments.of("0 + 1.", 6),
			Arguments.of("(2 + (34)", 9),
			Arguments.of("(2 + (34.))", 9)
		);
	}


	@ParameterizedTest
	@MethodSource("functionNotFoundExceptionTests")
	void functionNotFoundExceptionTest(String inputString, int position) {

		testCalculatorImplExceptions(inputString, position);
	}

	static Stream<Arguments> functionNotFoundExceptionTests() {

		return Stream.of(
			Arguments.of("5 + absd(1, 2, 3)", 17),
			Arguments.of("5 + dabs(1, 2, 3)", 17),
			Arguments.of("5 + okefeknf(1, 2, 3)", 21)
		);
	}


	@ParameterizedTest
	@MethodSource("wrongArgumentsCountExceptionTests")
	void wrongArgumentsCountExceptionTest(String inputString, int position) {

		testCalculatorImplExceptions(inputString, position);
	}

	static Stream<Arguments> wrongArgumentsCountExceptionTests() {

		return Stream.of(
			Arguments.of("abs(1, 2)", 9),
			Arguments.of("abs()", 5),
			Arguments.of("max()", 5),
			Arguments.of("pi(1, 2, 3)", 11)
		);
	}


	@ParameterizedTest
	@MethodSource("operatorNotFoundExceptionTests")
	void operatorNotFoundExceptionTest(String inputString, int position) {

		testCalculatorImplExceptions(inputString, position);
	}

	static Stream<Arguments> operatorNotFoundExceptionTests() {

	    return Stream.of(
		    Arguments.of("1 & 3 + 1", 8),
		    Arguments.of("1 | 3", 4),
		    Arguments.of("1 $ 3", 4),
		    Arguments.of("1 @ 3", 4),
		    Arguments.of("1 ? 3", 4)
	    );
	}

	@ParameterizedTest
	@MethodSource("numbersTests")
	void numbersTest(double expectedResult, String inputString) throws MollanException {

		testCalculatorImpl(expectedResult, inputString);
	}

	static Stream<Arguments> numbersTests() {

		return Stream.of(

			// Positive integer number
			Arguments.of(0.0, "0"),
			Arguments.of(1.0, "1"),
			Arguments.of(10.0, "10"),
			Arguments.of(15.0, "15"),
			Arguments.of(12345.0, "12345"),

			// Positive decimal number
			Arguments.of(0.0, "0.0"),
			Arguments.of(1.0, "1.0"),
			Arguments.of(1.5, "1.5"),
			Arguments.of(1.25, "1.25"),
			Arguments.of(15.25, "15.25"),

			// Negative integer number
			Arguments.of(-0.0, "-0"),
			Arguments.of(-1.0, "-1"),
			Arguments.of(-12345.0, "-12345"),

			// Negative decimal number
			Arguments.of(-0.0, "-0.0"),
			Arguments.of(-1.0, "-1.0"),
			Arguments.of(-1.5, "-1.5"),
			Arguments.of(-1.25, "-1.25"),
			Arguments.of(-15.25, "-15.25")
		);
	}


	@ParameterizedTest
	@MethodSource("doubleOperatorsTests")
	void doubleOperatorsTest(double expectedResult, String inputString) throws MollanException {

		testCalculatorImpl(expectedResult, inputString);
	}

	static Stream<Arguments> doubleOperatorsTests() {

		return Stream.of(

			// Additional operator
			Arguments.of(0.0, "0 + 0"),
			Arguments.of(3.0, "1 + 2"),
			Arguments.of(5.0, "3 + 2"),
			Arguments.of(5.0, "2.5 + 2.5"),
			Arguments.of(7.25, "5 + 2.25"),

			// Subtraction operator
			Arguments.of(0.0, "0 - 0"),
			Arguments.of(-1.0, "1 - 2"),
			Arguments.of(1.0, "3 - 2"),
			Arguments.of(0.5, "3 - 2.5"),
			Arguments.of(0.0, "2.5 - 2.5"),
			Arguments.of(2.75, "5 - 2.25"),

			// Multiplication operator
			Arguments.of(20.0, "1 * 20"),
			Arguments.of(20.0, "4 * 5"),
			Arguments.of(5.0, "2.5 * 2"),
			Arguments.of(3.3 * 3, "3.3 * 3"),
			Arguments.of(10.0, "20 * 0.5"),
			Arguments.of(20.0, "20 * 0.5 * 2"),

			// Division operator
			Arguments.of(2.0, "2 / 1"),
			Arguments.of(0.5, "1 / 2"),
			Arguments.of(5.0, "20 / 2 / 2"),
			Arguments.of(20.0, "10 / 0.5"),

			// Power operator
			Arguments.of(4.0, "2 ^ 2"),
			Arguments.of(1.0, "1 ^ 2000"),
			Arguments.of(1.0, "20000 ^ 0"),
			Arguments.of(4.0, "16 ^ 0.5"),

			// OperatorFactory priority
			Arguments.of(6.0, "2 + 2 * 2"),
			Arguments.of(33.0, "12 + 34 - 6.5 * 2"),
			Arguments.of(5.75, "1 + 2.25 * 3 - 2"),
			Arguments.of(-0.25, "1 + 2.25 * 3 - 2^3")
		);
	}


	@ParameterizedTest
	@MethodSource("bracketsTests")
	void bracketsTest(double expectedResult, String inputString) throws MollanException {

		testCalculatorImpl(expectedResult, inputString);
	}

	static Stream<Arguments> bracketsTests() {

		return Stream.of(
			Arguments.of(1.0, "(1)"),
			Arguments.of(5.0, "(1 + 4)"),
			Arguments.of(6.0, "1 + (2 + 3)"),
			Arguments.of(5.0, "1 * (2 + 3)"),
			Arguments.of(6.0, "(2 + 3) + 1"),
			Arguments.of(6.0, "(1 + 2) + 3"),
			Arguments.of(32.0, "(2 + 2) * (2 + 2) * 2"),
			Arguments.of(29.0, "(5 + 6 * (3 + 1))")
		);
	}


	@ParameterizedTest
	@MethodSource("functionsTests")
	void functionsTest(double expectedResult, String inputString) throws MollanException {

		testCalculatorImpl(expectedResult, inputString);
	}

	static Stream<Arguments> functionsTests() {

		return Stream.of(
			Arguments.of(1.0, "abs(1)"),
			Arguments.of(1.0, "abs(-1)"),
			Arguments.of(18.0, "(1 + 2) * abs(-3 * (4 / 2))"),
			Arguments.of(20.0, "max(1, 2, 3, 4, 5, 15) + 5"),
			Arguments.of(Math.PI + 1, "pi() + 1"),
			Arguments.of(999903.0, "sum(min(1 * 3, log2(8) - 100, pi()), max(sum(10, 24), 100^3))")
		);
	}
}