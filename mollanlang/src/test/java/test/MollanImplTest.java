package test;

import com.google.common.truth.Truth;
import mollan.Mollan;
import mollan.compilers.MollanException;
import mollan.impl.MollanImpl;
import mollan.runtime.RuntimeEnvironment;
import mollan.runtime.types.ValueHolder;
import mollan.runtime.types.booleantype.BooleanValueHolder;
import mollan.runtime.types.doubletype.DoubleValueHolder;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

@SuppressWarnings("ALL")
class MollanImplTest {

	private RuntimeEnvironment execute(String inputString) throws MollanException {

		Mollan mollan = new MollanImpl();

		RuntimeEnvironment runtimeEnvironment = new RuntimeEnvironment();

		mollan.execute(inputString, runtimeEnvironment);

		return runtimeEnvironment;
	}

	/**
	 * Method for assert error position. In case runtimeexception put position -1.<br>
	 */
	private void assertErrorPosition(String inputString, int position) {

		try {

			this.execute(inputString);

		} catch (MollanException mollanException) {

			Truth.assertWithMessage("test exception %s (%s)",
					mollanException.getClass().getSimpleName(),
					mollanException.getMessage())
				.that(mollanException.getPosition())
				.isEqualTo(position);

		} catch (RuntimeException runtimeException) {

			Truth.assertWithMessage("test runtime exception (%s)", runtimeException.getMessage())
				.that(-1).isEqualTo(position);
		}
	}

	@ParameterizedTest
	@MethodSource("variablesTests")
	void variablesTest(String inputString, String variableName, ValueHolder<?> expectedVariableValue) throws MollanException {

		RuntimeEnvironment runtimeEnvironment = this.execute(inputString);

		Truth.assertThat(runtimeEnvironment.getVariable(variableName).get().value())
			.isEqualTo(expectedVariableValue.value());
	}

	static Stream<Arguments> variablesTests() {

	    return Stream.of(
		    Arguments.of("a = 123; b = 456;", "a", new DoubleValueHolder(123.0)),
		    Arguments.of("a = 1+2*3; b = 2*(2+1+a);", "b", new DoubleValueHolder(20.0)),
		    Arguments.of("a = -2; b = 1 + abs(a);", "b", new DoubleValueHolder(3.0)),
	        Arguments.of("a = 10; b = 5; a = a * b;", "a", new DoubleValueHolder(50.0)),
		    Arguments.of("a = 15; a = 2; a = a^a;", "a", new DoubleValueHolder(4.0)),
		    Arguments.of("a = 10>5; b = 10<5;", "a", new BooleanValueHolder(true))
	    );
	}


	@ParameterizedTest
	@MethodSource("printProcedureTests")
	void printProcedureTest(String inputString, String expectedOutput) throws MollanException {

		RuntimeEnvironment runtimeEnvironment = this.execute(inputString);

		Truth.assertThat(runtimeEnvironment.getOutput())
			.isEqualTo(expectedOutput);
	}

	static Stream<Arguments> printProcedureTests() {

	    return Stream.of(
		    Arguments.of("a = -2; b = 1 + abs(a); print(b+1); print(a);", "4.0, -2.0"),
		    Arguments.of("a = 5>=5; print(a);", "true"),
		    Arguments.of("a = 1; b = 5; a = 3; print(b>a);", "true"),
		    Arguments.of("print(abs(sqrt(1 + 2^3) - 6) < 5);", "true")
	    );
	}


	@ParameterizedTest
	@MethodSource("booleanOperatorsTests")
	void booleanOperatorsTest(String inputString, boolean expectedResult) throws MollanException {

		RuntimeEnvironment runtimeEnvironment = this.execute(inputString);

		Truth.assertThat(runtimeEnvironment.getVariable("result").get().value())
			.isEqualTo(expectedResult);
	}

	static Stream<Arguments> booleanOperatorsTests() {

		return Stream.of(
			Arguments.of("result = 5 > 3;", true),
			Arguments.of("result = 10 < 11;", true),
			Arguments.of("result = 3 >= 2;", true),
			Arguments.of("result = 5+3 > 2*2+2;", true)
		);
	}


	@ParameterizedTest
	@MethodSource("forLoopTests")
	void forLoopTest(String inputString, String expectedOutput) throws MollanException {

		RuntimeEnvironment runtimeEnvironment = execute(inputString);

		Truth.assertThat(runtimeEnvironment.getOutput())
			.isEqualTo(expectedOutput);
	}

	static Stream<Arguments> forLoopTests() {

		return Stream.of(
			Arguments.of("for(i = 0; i < 5; i = i + 1) { print(i); };", "0.0, 1.0, 2.0, 3.0, 4.0"),
			Arguments.of("for(i = 0; i < 3; i = i + 1) { for(j = 0; j < i; j = j + 1) { print(j); }; print(i); };",
				"0.0, 0.0, 1.0, 0.0, 1.0, 2.0"),
			Arguments.of("for(i = 1; i < 1; i = i + 1) { print(i); };", ""),
			Arguments.of("sum = 0; for(i = 0; i < 10; i = i + 1) { sum = sum + i; }; print(sum);", "45.0")
		);
	}


	@ParameterizedTest
	@MethodSource("switchTests")
	void switchTest(String inputString, String output) throws MollanException {

		RuntimeEnvironment runtimeEnvironment = this.execute(inputString);

		Truth.assertWithMessage("switch test {} ({})")
			.that(runtimeEnvironment.getOutput())
			.isEqualTo(output);
	}

	static Stream<Arguments> switchTests() {

	    return Stream.of(

	    	Arguments.of(
	        	"a = 5; switch (a) { case 1: { print(1); } case 2: { print(2); } default: { print(3); } };",
		        "3.0"),

		    Arguments.of(
		    	"a = 5; switch (a) { case 1: { print(1); } case 5: { print(3); } case 2: { print(2); } };",
			    "3.0"),

		    Arguments.of(
		    	"a = 5; switch (a) { case 5: { b = 10; switch (b) { case 10: { print(a); } }; } }; print(b);",
			    "5.0, 10.0")
	    );
	}

	@ParameterizedTest
	@MethodSource("ifStatementTests")
	void ifStatementTest(String inputString, String output) throws MollanException {

		RuntimeEnvironment runtimeEnvironment = this.execute(inputString);

		Truth.assertWithMessage("'if' statement test {} ({})")
			.that(runtimeEnvironment.getOutput())
			.isEqualTo(output);
	}

	static Stream<Arguments> ifStatementTests() {

	    return Stream.of(
	        Arguments.of("if (5 > 3) { print(1); };", "1.0"),
		    Arguments.of("a = 5 > 4; if (a) { print(1); };", "1.0"),
		    Arguments.of("if (5 < 3) { print(1); };", ""),
		    Arguments.of("if (5 > 3) { if (99 < 99 + 1) { print(1); }; print(2); };", "1.0, 2.0")
	    );
	}

	@ParameterizedTest
	@MethodSource("variableNotFoundExceptionTests")
	void variableNotFoundExceptionTest(String inputString) {

		assertErrorPosition(inputString, -1);
	}

	static Stream<Arguments> variableNotFoundExceptionTests() {

	    return Stream.of(
	        Arguments.of("a = 5; b = a + b;"),
	        Arguments.of("a = b;"),
		    Arguments.of("print(a);")
	    );
	}


	@ParameterizedTest
	@MethodSource("procedureNotFoundExceptionTests")
	void procedureNotFoundExceptionTest(String inputString, int position) {

		assertErrorPosition(inputString, position);
	}

	static Stream<Arguments> procedureNotFoundExceptionTests() {

	    return Stream.of(
	        Arguments.of("printf(123);", 11),
		    Arguments.of("fprint(123);", 11),
		    Arguments.of("kek();", 5)
	    );
	}

	@ParameterizedTest
	@MethodSource("switchExceptionTests")
	void switchExceptionTest(String inputString, int position) {

		this.assertErrorPosition(inputString, position);
	}

	static Stream<Arguments> switchExceptionTests() {

	    return Stream.of(
	        Arguments.of("switch (a) {};", -1),
		    Arguments.of("switch (1) {};", 8),
		    Arguments.of("a = 5; switch (a+1) {};", 16),
		    Arguments.of("a = 5; switch (a) { case 1+2: {} };", 26),
		    Arguments.of("a = 5; switch (a) { case 5: { print(1); };", 41),
		    Arguments.of("a = 5; switch (a) { case 5: { print(1); } default: { print(2); } case 10: { print(3); } };", 65)
	    );
	}

	@ParameterizedTest
	@MethodSource("ifStatementExceptionTests")
	void ifStatementExceptionTest(String inputString, int position) {

		this.assertErrorPosition(inputString, position);
	}

	static Stream<Arguments> ifStatementExceptionTests() {

	    return Stream.of(
	        Arguments.of("if (a) { print(1); };", -1),
		    Arguments.of("if (5 + 1) { print(1); };", 4),
		    Arguments.of("if (5 > 1);", 10),
		    Arguments.of("if () {};", 4)
	    );
	}
}
