<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Mollan</title>
</head>
<body>

<script>

  function mollanClick() {

    const source = document.getElementById("mollanSource").value;

    sendPostRequest(source).then(body => document.getElementById("mollanOutput").innerHTML = body);
  }

  async function sendPostRequest(source) {

    const port = location.port;

    const response = await fetch("/execute", {
      method: "POST",
      cache: "no-cache",
      headers: {"Content-Type": "application/json"},
      body: source
    });

    return await response.text();
  }

</script>

  <h1>Write your Mollan code:</h1>
  <label>
    <textarea id="mollanSource" rows="10" style="width: 500px;"></textarea>
  </label>
  <br>
  <button id="mollanButton" type="button" onclick="mollanClick();">Execute</button>

  <br>
  <br>
  <h3>Output:</h3>
  <span id="mollanOutput">Empty</span>

</body>
</html>