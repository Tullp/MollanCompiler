package page;

import mollan.Mollan;
import mollan.compilers.MollanException;
import mollan.impl.MollanImpl;
import mollan.runtime.RuntimeEnvironment;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Collectors;

import static spark.Spark.*;

public class Main {

	public static void main(String[] args) {

		get("/", (req, resp) -> new String(Files.readAllBytes(Paths.get(
			"C:\\D\\java\\MollanCompiler\\spark\\src\\main\\java\\page\\index.jsp"
		))));

		post("/execute", (request, response) -> {

			String source = request.body();

			RuntimeEnvironment runtimeEnvironment = new RuntimeEnvironment();

			try {

				new MollanImpl().execute(source, runtimeEnvironment);

				return runtimeEnvironment.getOutput();

			} catch (MollanException error) {

				return error.getMessage();
			}
		});
	}
}
